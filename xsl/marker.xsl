<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:mrk="https://www.igsg.cnr.it/marker/"
	xmlns:lkn="https://www.igsg.cnr.it/linkoln/"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.org/HTML/1998/html4"
	xmlns:h="http://www.w3.org/1999/xhtml"
	xmlns:xlink="http://www.w3.org/1999/xlink"
	xmlns:date="http://exslt.org/dates-and-times">


	<xsl:output method="html" indent="yes" />


	<xsl:template match="/">
		<html>
			<head>
				<title>IGSG Marker</title>
				<link href="./../xsl/marker.css" rel="stylesheet" />
			</head>
			<body>


				<table width="80%" border="0" align="center">
					<tr>
						<td width="50%">
							<xsl:apply-templates />
						</td>
					</tr>
				</table>

			</body>
		</html>
	</xsl:template>

	<xsl:variable name="source" select="//*[name()='root']/@source">
	</xsl:variable>

	<!-- Se è presente l'attributo URL crea un <a>, altrimenti <font color blue> -->
	<xsl:template match='lkn:ref[@url]'>
		<a href="{./@url}">
			<xsl:apply-templates />	
		</a>
	</xsl:template>

	<xsl:template match='lkn:ref'>
		<font color='blue'>
			<xsl:apply-templates />	
		</font>
	</xsl:template>

	<xsl:template match='lkn:warning'>
		<font color='red'>
			<xsl:apply-templates />	
		</font>
	</xsl:template>


	<!-- ======================================================== -->
	<!-- -->
	<!-- Template articolato -->
	<!-- -->
	<!-- ======================================================== -->

	<xsl:template match='mrk:corpo'>
		<div>
			<xsl:apply-templates />
		</div>
	</xsl:template>
	
	
	<xsl:template match='h:p'>
		<p style= "{./@style}">
			<xsl:apply-templates />
		</p>
	</xsl:template>
	
	<xsl:template match='h:br'>	
		<br/>
	</xsl:template>
	
	<xsl:template match='h:table'>
		<a name="{@id}"></a>
		<table class="table">
			<xsl:apply-templates />
		</table>
	</xsl:template>
	
	<xsl:template match='h:th'>
		<th class = "th">
			<xsl:apply-templates />
		</th>
	</xsl:template>
	
	<xsl:template match='h:tr'>
		<tr class = "tr">
			<xsl:apply-templates />
		</tr>
	</xsl:template>
	
	<xsl:template match='h:td'>
		<td class = "td" style= "{./@style}" colspan= "{./@colspan}" rowspan= "{./@rowspan}">
			<xsl:apply-templates />
		</td>
	</xsl:template>
	
	<xsl:template match='h:pre'>
		<pre>
			<xsl:apply-templates />
		</pre>
	</xsl:template>

	<xsl:template match='mrk:testa'>	
		<div class="header">
			<xsl:apply-templates />
		</div>
	</xsl:template>

	<xsl:template match='mrk:emananteIntestazione'>
		<div class="emananteDoc">
			<xsl:apply-templates />
		</div>
	</xsl:template>

	<xsl:template match='mrk:tipoDoc'>
		<span class="documentType">
			<xsl:apply-templates />
		</span>
	</xsl:template>
	
	<xsl:template match='mrk:numeroDoc'>
		<span class="documentNumber">
			<xsl:apply-templates />
		</span>
	</xsl:template>
	
	<xsl:template match='mrk:codiceDoc'>
		<span class="documentCode">
			<xsl:apply-templates />
		</span>
	</xsl:template>
	
	<xsl:template match='mrk:dataDoc'>
		<span class="documentDate">
			<xsl:apply-templates />
		</span>
	</xsl:template>
	
	<xsl:template match='mrk:titoloDoc'>
		<div class="documentTitle">
			<xsl:apply-templates />
		</div>
	</xsl:template>
	
	<xsl:template match='mrk:preambolo'>
		<div class="preamble">
			<xsl:apply-templates />
		</div>
	</xsl:template>
	
	
	<!-- ========================== LIBRO ============================== -->
	<xsl:template match='mrk:libro'>
		<a name="{@id}"></a>
		<div class="libro">
			<xsl:apply-templates />
		</div>
	</xsl:template>

	<!-- ========================== PARTE ============================== -->
	<xsl:template match='mrk:parte'>
		<a name="{@id}"></a>
		<div class="parte">
			<xsl:apply-templates />
		</div>
	</xsl:template>

	<!-- ========================== TITOLO ============================== -->
	<xsl:template match='mrk:titolo'>
		<a name="{@id}"></a>
		<div class="titolo">
			<xsl:apply-templates />
		</div>
	</xsl:template>

	<!-- ========================== SEZIONE ============================== -->
	<xsl:template match='mrk:sezione'>
		<a name="{@id}"></a>
		<div class="sezione">
			<xsl:apply-templates />
		</div>
	</xsl:template>

	<!-- ========================== CAPO ============================== -->
	<xsl:template match='mrk:capo'>
		<a name="{@id}"></a>
		<div class="capo">
			<xsl:apply-templates />
		</div>
	</xsl:template>

	<!-- ========================== RUBRICA ============================== -->

	<xsl:template match='mrk:rubrica'>
		<a name="{@id}"></a>
		<p class="rubrica">
			<xsl:apply-templates />
		</p>
	</xsl:template>


	<!-- ======================== INTESTAZIONE =========================== -->


	<xsl:template match='mrk:intestazione'>
	  <xsl:element name="span">
		<xsl:attribute name="class">
			<xsl:value-of select ="concat('intestazione_',local-name(./..))"/>
		</xsl:attribute>
		<xsl:attribute name="title"><xsl:value-of select="./../@id" /></xsl:attribute>
			
		<xsl:apply-templates />	
	  </xsl:element>
	</xsl:template>
	
	



	<!-- ========================= ARTICOLO =============================== -->

	<xsl:template match='mrk:articolo'>
		<a name="{@id}"></a>
		<div class="articolo">
			<xsl:apply-templates />
		</div>
	</xsl:template>


	<!-- ========================= COMMA e sotto comma =============================== -->

	<xsl:template match='mrk:comma'>
		<a name="{@id}"></a>
		<div class="comma">
			<xsl:apply-templates />
		</div>
	</xsl:template>
	


	
	<xsl:template match='mrk:paragrafo'>
		<a name="{@id}"></a>
		<xsl:choose>
		    <!-- primo paragrafo di un comma numerato: non andare a capo -->
			<xsl:when test="name(preceding-sibling::*[1])='mrk:intestazione' and name(./..)='mrk:comma'" >
				<span>
					<xsl:apply-templates />
				</span>
			</xsl:when>
			<xsl:otherwise>
			   <xsl:choose>
			        <!-- @id ends-with "paragraph-i" -->
					<xsl:when test="'paragraph-i'=substring(@id, string-length(@id) - string-length('paragraph-i') +1)" >
						<p class="primo_paragrafo">
							<xsl:apply-templates />
						</p>
					</xsl:when>
			       <xsl:otherwise>
			            <p class="paragrafo">
							<xsl:apply-templates />
						</p>
			       </xsl:otherwise>
			    </xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	

	

	
	<!-- ========================= EL , EN , EP =============================== -->
	<xsl:template match='mrk:lettera'>
		<a name="{@id}"></a>
		<p class="lettera">
			<xsl:apply-templates />
		</p>
	</xsl:template>

	<xsl:template match='mrk:numero'>
		<a name="{@id}"></a>
		<p class="numero">
			<xsl:apply-templates />
		</p>
	</xsl:template>
	
	
	<xsl:template match='mrk:sottoNumero'>
		<a name="{@id}"></a>
		<p class="sottoNumero">
			<xsl:apply-templates />
		</p>
	</xsl:template>
	
	<xsl:template match='mrk:numeroLatino'>
		<a name="{@id}"></a>
		<p class="numeroLatino">
			<xsl:apply-templates />
		</p>
	</xsl:template>


	<xsl:template match='mrk:virgolette'>
		<xsl:choose>
			<xsl:when test="*" >
				<div class="virgolette_struttura">
					<xsl:apply-templates />
				</div>		
			</xsl:when>
			<xsl:otherwise>
				<span class="virgolette_parola">
					<xsl:apply-templates />
				</span>
			</xsl:otherwise>
		</xsl:choose>	
	</xsl:template>

	<xsl:template match='mrk:parole'>
	  <xsl:element name="span">
		<xsl:attribute name="class">parole</xsl:attribute>
		<xsl:attribute name="title">Parole tra virgolette</xsl:attribute>
		<xsl:apply-templates />	
	  </xsl:element>
	</xsl:template>

	<xsl:template match='mrk:parole_inserimento'>
	  <xsl:element name="span">
		<xsl:attribute name="class">inserimento</xsl:attribute>
		<xsl:attribute name="title">Modifica di parole - Parole da inserire</xsl:attribute>
		<xsl:apply-templates />	
	  </xsl:element>
	</xsl:template>

	<xsl:template match='mrk:parole_sostituzione'>
	  <xsl:element name="span">
		<xsl:attribute name="class">sostituzione</xsl:attribute>
		<xsl:attribute name="title">Modifica di parole - Parole da sostituire</xsl:attribute>
		<xsl:apply-templates />	
	  </xsl:element>
	</xsl:template>

	<xsl:template match='mrk:parole_soppressione'>
	  <xsl:element name="span">
		<xsl:attribute name="class">soppressione</xsl:attribute>
		<xsl:attribute name="title">Modifica di parole - Parole soppresse</xsl:attribute>
		<xsl:apply-templates />	
	  </xsl:element>
	</xsl:template>

	<xsl:template match='mrk:parole_dopo'>
	  <xsl:element name="span">
		<xsl:attribute name="class">dopo</xsl:attribute>
		<xsl:attribute name="title">Modifica di parole - Punto di battuta</xsl:attribute>
		<xsl:apply-templates />	
	  </xsl:element>
	</xsl:template>


	<xsl:template match='mrk:skip'>
			<div class="virgolette_parola">
				<xsl:apply-templates />
			</div>	
	</xsl:template>


	<xsl:template match='mrk:conclusione'>
			<div class="footer">
				<xsl:apply-templates />
			</div>	
	</xsl:template>


    <!-- mrk:allegato id="annex-2"><mrk:intestazione>ALLEGATO II</mrk:intestazione>   -->


	<xsl:template match='mrk:allegato'>
		<hr />
		<a name="{@id}"></a>
		<xsl:choose>
			<!-- 1. allegato con struttura  -->
			<xsl:when test=".//mrk:corpo" >
					<xsl:apply-templates />
			</xsl:when>
			<xsl:otherwise>
				<div class="allegato">
					<xsl:apply-templates />
				</div>		
			</xsl:otherwise>
		</xsl:choose>			
	</xsl:template>





	<!-- ========================= MARKER METADATA - LOG ========================= -->


	<xsl:template match='mrk:metadata'>
		<table class="metadata_table">
			<xsl:apply-templates />
			<xsl:call-template name="validationError"/>
			<xsl:call-template name="notWellFormattedError"/>
		</table>
	</xsl:template>


	<xsl:template match='mrk:info'>
		<tr>
			<td class="mrk_info">
				<xsl:apply-templates />
			</td>
		</tr>
	</xsl:template>
	
	<xsl:template match='mrk:eli'>
		<tr>
			<td class="mrk_eli">
				<a class="anchor" href="{.}" target="_blank"><xsl:value-of select="."/></a>
			</td>
		</tr>
	</xsl:template>
	
	<xsl:template match='mrk:vigenza'>
		<tr>
			<td class="mrk_vigenza">
				 <xsl:apply-templates />
			</td>
		</tr>
	</xsl:template>
	
	<xsl:template match='mrk:message'>
		<tr>
			<td class="mrk_message">
				<xsl:apply-templates />
			</td>
		</tr>
	</xsl:template>

	<!--  
	<xsl:template match='mrk:error'>
		<xsl:variable name="idref">
			<xsl:value-of select="@idref" />
		</xsl:variable>
		<tr>
		    <xsl:if test="$idref!=''">
					<a class="anchor" href="#{@idref}" title="vai a: {@idref}">&#x2913;&#160;</a>
			</xsl:if>
			<td class="mrk_warning">
				<xsl:apply-templates />
			</td>
		</tr>
	</xsl:template>
	-->
	
	<xsl:template match='mrk:error'>
		<xsl:variable name="idref">
			<xsl:value-of select="@idref" />
		</xsl:variable>
		<tr>
			<td class="mrk_error">
				<xsl:if test="$idref!=''">
					<a class="anchor" href="#{@idref}" title="vai a: {@idref}">&#x2913;&#160;</a>
				</xsl:if>
				<xsl:apply-templates />
			</td>
		</tr>
	</xsl:template>
	
	
	<!-- &#x2304; &#x2913; &#x23EC;  &#x261F;   -->
	<xsl:template match='mrk:warning'>
		<xsl:variable name="idref">
			<xsl:value-of select="@idref" />
		</xsl:variable>
		<tr>
			<td class="mrk_warning">
				<xsl:if test="$idref!=''">
					<a class="anchor" href="#{@idref}" title="vai a: {@idref}">&#x2913;&#160;</a>
				</xsl:if>
				<xsl:apply-templates />
			</td>
		</tr>
	</xsl:template>
	
	
	<xsl:template name= 'validationError'>
			<tr>
				<td class="mrk_error">
					<xsl:value-of select="//mrk:validationError"/>
				</td>
			</tr>
	</xsl:template>
	
	<xsl:template match='mrk:validationError'/>

	<xsl:template name= 'notWellFormattedError'>
			<tr>
				<td class="mrk_error">
					<xsl:value-of select="//mrk:notWellFormattedError"/>
				</td>
			</tr>
	</xsl:template>
	
	<xsl:template match='mrk:notWellFormattedError'/>


</xsl:stylesheet>