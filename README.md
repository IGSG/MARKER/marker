<img src="igsg-marker-black.png" alt="IGSG Marker logo" width="400"/>

<hr>

**IGSG Marker** is a software developed at the [Institute of Legal Informatics and Judicial Systems](https://www.igsg.cnr.it) (IGSG) of the [National Research Council of Italy](https://www.cnr.it/) (CNR) for the automatic parsing of Italian legislative texts.

<h2>More info available at [https://igsg-marker.gitlab.io](https://igsg-marker.gitlab.io)</h2>

<p>&nbsp;</p>
<p>&nbsp;</p>

<img src="marker-legge-html-modifiche-linkoln.png" alt="IGSG Marker and Linkoln screenshot"/>


