// DO NOT EDIT
// Generated by JFlex 1.9.1 http://jflex.de/
// source: jflex/PreGazzettaSuper.jflex

/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.marker;


@SuppressWarnings("fallthrough")
public class PreGazzettaSuper extends JFlexAnnotationService {

  /** This character denotes the end of file. */
  public static final int YYEOF = -1;

  /** Initial size of the lookahead buffer. */
  private static final int ZZ_BUFFERSIZE = 16384;

  // Lexical states.
  public static final int YYINITIAL = 0;
  public static final int inSuper = 2;

  /**
   * ZZ_LEXSTATE[l] is the state in the DFA for the lexical state l
   * ZZ_LEXSTATE[l+1] is the state in the DFA for the lexical state l
   *                  at the beginning of a line
   * l is of the form l = 2*k, k a non negative integer
   */
  private static final int ZZ_LEXSTATE[] = {
     0,  0,  1, 1
  };

  /**
   * Top-level table for translating characters to character classes
   */
  private static final int [] ZZ_CMAP_TOP = zzUnpackcmap_top();

  private static final String ZZ_CMAP_TOP_PACKED_0 =
    "\1\0\1\u0100\34\u0200\1\u0300\1\u0200\1\u0400\267\u0200\10\u0500"+
    "\u1020\u0200";

  private static int [] zzUnpackcmap_top() {
    int [] result = new int[4352];
    int offset = 0;
    offset = zzUnpackcmap_top(ZZ_CMAP_TOP_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackcmap_top(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }


  /**
   * Second-level tables for translating characters to character classes
   */
  private static final int [] ZZ_CMAP_BLOCKS = zzUnpackcmap_blocks();

  private static final String ZZ_CMAP_BLOCKS_PACKED_0 =
    "\11\0\1\1\1\2\1\3\1\4\1\5\22\0\1\6"+
    "\5\0\1\7\7\0\1\10\1\0\1\11\1\12\10\11"+
    "\1\13\1\14\1\15\1\0\1\16\2\0\1\17\1\20"+
    "\1\21\1\11\1\22\2\11\1\23\1\24\2\11\1\25"+
    "\1\26\1\27\1\30\1\31\1\11\1\32\1\33\1\34"+
    "\5\11\1\35\3\0\1\36\2\0\1\37\1\20\1\40"+
    "\1\11\1\22\2\11\1\23\1\24\2\11\1\41\1\26"+
    "\1\27\1\42\1\43\1\11\1\32\1\44\1\45\5\11"+
    "\1\35\12\0\1\3\44\0\1\36\5\0\1\36\11\0"+
    "\1\36\5\0\6\11\1\0\11\11\1\0\6\11\2\0"+
    "\4\11\3\0\6\11\1\0\11\11\1\0\6\11\2\0"+
    "\4\11\53\0\2\11\6\0\2\46\66\0\2\11\4\0"+
    "\2\11\17\0\1\47\u0188\0\2\11\216\0\2\11\42\0"+
    "\2\11\104\0\1\1\45\0\2\3\326\0\u0100\3";

  private static int [] zzUnpackcmap_blocks() {
    int [] result = new int[1536];
    int offset = 0;
    offset = zzUnpackcmap_blocks(ZZ_CMAP_BLOCKS_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackcmap_blocks(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }

  /**
   * Translates DFA states to action switch labels.
   */
  private static final int [] ZZ_ACTION = zzUnpackAction();

  private static final String ZZ_ACTION_PACKED_0 =
    "\2\0\1\1\2\2\1\1\4\3\25\0\1\4\11\0"+
    "\1\4\100\0\2\5\1\0\2\6\5\0\2\7\37\0";

  private static int [] zzUnpackAction() {
    int [] result = new int[149];
    int offset = 0;
    offset = zzUnpackAction(ZZ_ACTION_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackAction(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }


  /**
   * Translates a state to a row index in the transition table
   */
  private static final int [] ZZ_ROWMAP = zzUnpackRowMap();

  private static final String ZZ_ROWMAP_PACKED_0 =
    "\0\0\0\50\0\120\0\120\0\170\0\240\0\120\0\310"+
    "\0\360\0\u0118\0\u0140\0\310\0\360\0\u0168\0\u0190\0\u01b8"+
    "\0\u01e0\0\u0208\0\u0230\0\u0258\0\u0280\0\u02a8\0\u02d0\0\u02f8"+
    "\0\u0320\0\u0348\0\u0370\0\u0398\0\u03c0\0\u03e8\0\u0410\0\u0438"+
    "\0\u0460\0\u0488\0\u04b0\0\u04d8\0\u0500\0\u0528\0\u0550\0\u0578"+
    "\0\u05a0\0\120\0\u05c8\0\u05f0\0\u0618\0\u0640\0\u0668\0\u0690"+
    "\0\u06b8\0\u06e0\0\u0708\0\u0730\0\u0758\0\u0780\0\u07a8\0\u07d0"+
    "\0\u07f8\0\u0820\0\u0848\0\u0870\0\u0898\0\u08c0\0\u08e8\0\u0910"+
    "\0\u0938\0\u0960\0\u0988\0\u09b0\0\u09d8\0\u0a00\0\u0a28\0\u0a50"+
    "\0\u0a78\0\u0aa0\0\u0ac8\0\u0af0\0\u0b18\0\u0b40\0\u0b68\0\u0b90"+
    "\0\u0bb8\0\u0be0\0\u0c08\0\u0c30\0\u0c58\0\u0c80\0\u0ca8\0\u0cd0"+
    "\0\u0cf8\0\u0d20\0\u0d48\0\u0d70\0\u0d98\0\u0dc0\0\u0de8\0\u0e10"+
    "\0\u0e38\0\u0e60\0\u0e88\0\u0eb0\0\u0ed8\0\u0f00\0\u0f28\0\u0f50"+
    "\0\u0f78\0\u0fa0\0\120\0\u0fc8\0\u0ff0\0\u1018\0\120\0\u1040"+
    "\0\u1068\0\u1090\0\u10b8\0\u10e0\0\u1108\0\120\0\u1130\0\u1158"+
    "\0\u1180\0\u11a8\0\u11d0\0\u11f8\0\u1220\0\u1248\0\u1270\0\u1018"+
    "\0\u1298\0\u12c0\0\u12e8\0\u1108\0\u1310\0\u1338\0\u1360\0\u1388"+
    "\0\u13b0\0\u13d8\0\u1400\0\u1428\0\u1450\0\u1478\0\u14a0\0\u14c8"+
    "\0\u14f0\0\u1518\0\u1540\0\u1568\0\u1590";

  private static int [] zzUnpackRowMap() {
    int [] result = new int[149];
    int offset = 0;
    offset = zzUnpackRowMap(ZZ_ROWMAP_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackRowMap(String packed, int offset, int [] result) {
    int i = 0;  /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length() - 1;
    while (i < l) {
      int high = packed.charAt(i++) << 16;
      result[j++] = high | packed.charAt(i++);
    }
    return j;
  }

  /**
   * The transition table of the DFA
   */
  private static final int [] ZZ_TRANS = zzUnpacktrans();

  private static final String ZZ_TRANS_PACKED_0 =
    "\2\3\1\4\2\3\1\5\7\3\1\6\32\3\1\7"+
    "\2\10\1\7\3\10\1\11\5\7\1\12\32\7\52\0"+
    "\1\4\70\0\1\13\25\0\2\14\1\0\3\14\1\15"+
    "\5\0\1\16\61\0\1\17\43\0\1\20\37\0\1\21"+
    "\57\0\1\22\44\0\1\23\42\0\1\24\65\0\1\25"+
    "\11\0\1\25\17\0\1\26\67\0\1\27\10\0\1\27"+
    "\2\0\1\27\31\0\1\30\11\0\1\30\5\0\1\31"+
    "\2\0\1\31\1\0\1\31\1\32\6\0\1\33\62\0"+
    "\1\34\11\0\1\34\35\0\1\35\11\0\1\35\5\0"+
    "\1\36\2\0\1\36\1\0\1\36\1\37\6\0\1\40"+
    "\31\0\16\31\1\33\31\31\27\0\1\41\37\0\1\42"+
    "\1\0\1\43\3\0\1\44\3\0\1\45\1\0\1\46"+
    "\1\47\14\0\1\50\2\0\1\50\1\0\1\50\1\51"+
    "\6\0\1\52\45\0\1\14\33\0\16\36\1\40\31\36"+
    "\27\0\1\53\41\0\1\54\3\0\1\55\3\0\1\56"+
    "\1\0\1\57\1\60\33\0\1\61\61\0\1\62\34\0"+
    "\1\63\17\0\1\63\34\0\1\64\21\0\1\64\20\0"+
    "\1\65\17\0\1\65\32\0\1\66\51\0\1\67\21\0"+
    "\1\67\1\0\16\50\1\52\31\50\27\0\1\70\40\0"+
    "\1\71\46\0\1\72\17\0\1\72\34\0\1\73\21\0"+
    "\1\73\20\0\1\74\17\0\1\74\32\0\1\75\51\0"+
    "\1\76\21\0\1\76\34\0\1\77\10\0\1\77\2\0"+
    "\1\77\34\0\1\100\10\0\1\100\33\0\1\101\11\0"+
    "\1\101\24\0\1\102\61\0\1\103\52\0\1\104\46\0"+
    "\1\105\10\0\1\105\22\0\1\106\62\0\1\107\10\0"+
    "\1\107\2\0\1\107\31\0\1\110\11\0\1\110\24\0"+
    "\1\111\61\0\1\112\52\0\1\113\46\0\1\114\10\0"+
    "\1\114\33\0\1\115\11\0\1\115\12\0\1\116\1\0"+
    "\1\116\13\0\1\117\21\0\1\117\25\0\1\67\3\0"+
    "\1\120\11\0\1\120\3\0\1\67\33\0\1\121\51\0"+
    "\1\122\10\0\1\122\26\0\1\123\21\0\1\123\31\0"+
    "\1\124\11\0\1\124\40\0\1\125\10\0\1\125\2\0"+
    "\1\125\31\0\1\126\11\0\1\126\30\0\1\76\3\0"+
    "\1\127\11\0\1\127\3\0\1\76\33\0\1\130\51\0"+
    "\1\131\10\0\1\131\26\0\1\132\21\0\1\132\31\0"+
    "\1\133\11\0\1\133\21\0\1\31\34\0\1\116\2\0"+
    "\1\116\1\0\1\116\1\134\2\0\1\135\56\0\1\136"+
    "\16\0\1\136\10\0\1\137\2\0\1\137\1\0\1\137"+
    "\1\140\70\0\1\141\11\0\1\141\27\0\1\141\55\0"+
    "\1\142\11\0\1\142\32\0\1\143\13\0\1\143\37\0"+
    "\1\144\11\0\1\144\20\0\1\36\34\0\1\145\2\0"+
    "\1\145\1\0\1\145\1\146\70\0\1\147\11\0\1\147"+
    "\27\0\1\147\55\0\1\150\11\0\1\150\32\0\1\151"+
    "\13\0\1\151\35\0\1\152\20\0\5\153\1\154\3\153"+
    "\2\0\4\153\17\0\1\153\7\0\2\153\30\0\1\155"+
    "\11\0\1\155\6\0\1\137\2\0\1\137\1\0\1\137"+
    "\1\140\2\0\1\156\11\0\1\157\4\0\1\160\11\0"+
    "\1\160\33\0\1\161\21\0\1\162\2\0\1\162\1\0"+
    "\1\162\1\163\67\0\1\164\50\0\1\120\11\0\1\120"+
    "\21\0\1\50\34\0\1\145\2\0\1\145\1\0\1\145"+
    "\1\146\2\0\1\165\11\0\1\166\4\0\1\167\11\0"+
    "\1\167\33\0\1\170\21\0\1\171\2\0\1\171\1\0"+
    "\1\171\1\172\67\0\1\173\50\0\1\127\11\0\1\127"+
    "\25\0\1\174\31\0\1\153\72\0\1\175\13\0\1\175"+
    "\44\0\2\157\2\0\1\157\37\0\1\176\35\0\1\177"+
    "\30\0\1\162\2\0\1\162\1\0\1\162\1\163\2\0"+
    "\1\200\11\0\1\157\4\0\1\160\11\0\1\160\33\0"+
    "\1\201\42\0\1\120\63\0\2\166\2\0\1\166\37\0"+
    "\1\202\35\0\1\203\30\0\1\171\2\0\1\171\1\0"+
    "\1\171\1\172\2\0\1\204\11\0\1\166\4\0\1\167"+
    "\11\0\1\167\33\0\1\205\42\0\1\127\60\0\1\206"+
    "\10\0\1\206\2\0\1\206\30\0\1\116\11\0\1\116"+
    "\31\0\1\207\21\0\1\207\34\0\1\210\10\0\1\210"+
    "\2\0\1\210\20\0\1\211\53\0\1\212\21\0\1\212"+
    "\34\0\1\213\10\0\1\213\2\0\1\213\20\0\1\214"+
    "\60\0\1\215\11\0\1\215\32\0\1\216\52\0\1\217"+
    "\11\0\1\217\37\0\1\220\10\0\1\220\2\0\1\220"+
    "\26\0\1\221\52\0\1\222\11\0\1\222\37\0\1\223"+
    "\10\0\1\223\2\0\1\223\14\0\1\116\33\0\2\157"+
    "\4\0\42\157\14\0\1\137\64\0\1\224\11\0\1\224"+
    "\4\0\2\166\4\0\42\166\14\0\1\145\64\0\1\225"+
    "\11\0\1\225\20\0\1\162\47\0\1\171\33\0";

  private static int [] zzUnpacktrans() {
    int [] result = new int[5560];
    int offset = 0;
    offset = zzUnpacktrans(ZZ_TRANS_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpacktrans(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      value--;
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }


  /** Error code for "Unknown internal scanner error". */
  private static final int ZZ_UNKNOWN_ERROR = 0;
  /** Error code for "could not match input". */
  private static final int ZZ_NO_MATCH = 1;
  /** Error code for "pushback value was too large". */
  private static final int ZZ_PUSHBACK_2BIG = 2;

  /**
   * Error messages for {@link #ZZ_UNKNOWN_ERROR}, {@link #ZZ_NO_MATCH}, and
   * {@link #ZZ_PUSHBACK_2BIG} respectively.
   */
  private static final String ZZ_ERROR_MSG[] = {
    "Unknown internal scanner error",
    "Error: could not match input",
    "Error: pushback value was too large"
  };

  /**
   * ZZ_ATTRIBUTE[aState] contains the attributes of state {@code aState}
   */
  private static final int [] ZZ_ATTRIBUTE = zzUnpackAttribute();

  private static final String ZZ_ATTRIBUTE_PACKED_0 =
    "\2\0\2\11\2\1\1\11\3\1\25\0\1\1\11\0"+
    "\1\11\100\0\1\11\1\1\1\0\1\1\1\11\5\0"+
    "\1\1\1\11\37\0";

  private static int [] zzUnpackAttribute() {
    int [] result = new int[149];
    int offset = 0;
    offset = zzUnpackAttribute(ZZ_ATTRIBUTE_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackAttribute(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }

  /** Input device. */
  private java.io.Reader zzReader;

  /** Current state of the DFA. */
  private int zzState;

  /** Current lexical state. */
  private int zzLexicalState = YYINITIAL;

  /**
   * This buffer contains the current text to be matched and is the source of the {@link #yytext()}
   * string.
   */
  private char zzBuffer[] = new char[Math.min(ZZ_BUFFERSIZE, zzMaxBufferLen())];

  /** Text position at the last accepting state. */
  private int zzMarkedPos;

  /** Current text position in the buffer. */
  private int zzCurrentPos;

  /** Marks the beginning of the {@link #yytext()} string in the buffer. */
  private int zzStartRead;

  /** Marks the last character in the buffer, that has been read from input. */
  private int zzEndRead;

  /**
   * Whether the scanner is at the end of file.
   * @see #yyatEOF
   */
  private boolean zzAtEOF;

  /**
   * The number of occupied positions in {@link #zzBuffer} beyond {@link #zzEndRead}.
   *
   * <p>When a lead/high surrogate has been read from the input stream into the final
   * {@link #zzBuffer} position, this will have a value of 1; otherwise, it will have a value of 0.
   */
  private int zzFinalHighSurrogate = 0;

  /** Number of newlines encountered up to the start of the matched text. */
  private int yyline;

  /** Number of characters from the last newline up to the start of the matched text. */
  private int yycolumn;

  /** Number of characters up to the start of the matched text. */
  private long yychar;

  /** Whether the scanner is currently at the beginning of a line. */
  @SuppressWarnings("unused")
  private boolean zzAtBOL = true;

  /** Whether the user-EOF-code has already been executed. */
  @SuppressWarnings("unused")
  private boolean zzEOFDone;

  /* user code: */
	
	PreGazzettaSuper() { }
	
	private boolean art1found = false;
	
	private boolean readRubrica = false;
	
	private StringBuilder superPartitions = new StringBuilder(); /* intestazione partizioni superiori all'articolo */
	
	StringBuilder getSuperPartitions() { return superPartitions; }


  /**
   * Creates a new scanner
   *
   * @param   in  the java.io.Reader to read input from.
   */
  public PreGazzettaSuper(java.io.Reader in) {
    this.zzReader = in;
  }


  /** Returns the maximum size of the scanner buffer, which limits the size of tokens. */
  private int zzMaxBufferLen() {
    return Integer.MAX_VALUE;
  }

  /**  Whether the scanner buffer can grow to accommodate a larger token. */
  private boolean zzCanGrow() {
    return true;
  }

  /**
   * Translates raw input code points to DFA table row
   */
  private static int zzCMap(int input) {
    int offset = input & 255;
    return offset == input ? ZZ_CMAP_BLOCKS[offset] : ZZ_CMAP_BLOCKS[ZZ_CMAP_TOP[input >> 8] | offset];
  }

  /**
   * Refills the input buffer.
   *
   * @return {@code false} iff there was new input.
   * @exception java.io.IOException  if any I/O-Error occurs
   */
  private boolean zzRefill() throws java.io.IOException {

    /* first: make room (if you can) */
    if (zzStartRead > 0) {
      zzEndRead += zzFinalHighSurrogate;
      zzFinalHighSurrogate = 0;
      System.arraycopy(zzBuffer, zzStartRead,
                       zzBuffer, 0,
                       zzEndRead - zzStartRead);

      /* translate stored positions */
      zzEndRead -= zzStartRead;
      zzCurrentPos -= zzStartRead;
      zzMarkedPos -= zzStartRead;
      zzStartRead = 0;
    }

    /* is the buffer big enough? */
    if (zzCurrentPos >= zzBuffer.length - zzFinalHighSurrogate && zzCanGrow()) {
      /* if not, and it can grow: blow it up */
      char newBuffer[] = new char[Math.min(zzBuffer.length * 2, zzMaxBufferLen())];
      System.arraycopy(zzBuffer, 0, newBuffer, 0, zzBuffer.length);
      zzBuffer = newBuffer;
      zzEndRead += zzFinalHighSurrogate;
      zzFinalHighSurrogate = 0;
    }

    /* fill the buffer with new input */
    int requested = zzBuffer.length - zzEndRead;
    int numRead = zzReader.read(zzBuffer, zzEndRead, requested);

    /* not supposed to occur according to specification of java.io.Reader */
    if (numRead == 0) {
      if (requested == 0) {
        throw new java.io.EOFException("Scan buffer limit reached ["+zzBuffer.length+"]");
      }
      else {
        throw new java.io.IOException(
            "Reader returned 0 characters. See JFlex examples/zero-reader for a workaround.");
      }
    }
    if (numRead > 0) {
      zzEndRead += numRead;
      if (Character.isHighSurrogate(zzBuffer[zzEndRead - 1])) {
        if (numRead == requested) { // We requested too few chars to encode a full Unicode character
          --zzEndRead;
          zzFinalHighSurrogate = 1;
        } else {                    // There is room in the buffer for at least one more char
          int c = zzReader.read();  // Expecting to read a paired low surrogate char
          if (c == -1) {
            return true;
          } else {
            zzBuffer[zzEndRead++] = (char)c;
          }
        }
      }
      /* potentially more input available */
      return false;
    }

    /* numRead < 0 ==> end of stream */
    return true;
  }


  /**
   * Closes the input reader.
   *
   * @throws java.io.IOException if the reader could not be closed.
   */
  public final void yyclose() throws java.io.IOException {
    zzAtEOF = true; // indicate end of file
    zzEndRead = zzStartRead; // invalidate buffer

    if (zzReader != null) {
      zzReader.close();
    }
  }


  /**
   * Resets the scanner to read from a new input stream.
   *
   * <p>Does not close the old reader.
   *
   * <p>All internal variables are reset, the old input stream <b>cannot</b> be reused (internal
   * buffer is discarded and lost). Lexical state is set to {@code ZZ_INITIAL}.
   *
   * <p>Internal scan buffer is resized down to its initial length, if it has grown.
   *
   * @param reader The new input stream.
   */
  public final void yyreset(java.io.Reader reader) {
    zzReader = reader;
    zzEOFDone = false;
    yyResetPosition();
    zzLexicalState = YYINITIAL;
    int initBufferSize = Math.min(ZZ_BUFFERSIZE, zzMaxBufferLen());
    if (zzBuffer.length > initBufferSize) {
      zzBuffer = new char[initBufferSize];
    }
  }

  /**
   * Resets the input position.
   */
  private final void yyResetPosition() {
      zzAtBOL  = true;
      zzAtEOF  = false;
      zzCurrentPos = 0;
      zzMarkedPos = 0;
      zzStartRead = 0;
      zzEndRead = 0;
      zzFinalHighSurrogate = 0;
      yyline = 0;
      yycolumn = 0;
      yychar = 0L;
  }


  /**
   * Returns whether the scanner has reached the end of the reader it reads from.
   *
   * @return whether the scanner has reached EOF.
   */
  public final boolean yyatEOF() {
    return zzAtEOF;
  }


  /**
   * Returns the current lexical state.
   *
   * @return the current lexical state.
   */
  public final int yystate() {
    return zzLexicalState;
  }


  /**
   * Enters a new lexical state.
   *
   * @param newState the new lexical state
   */
  public final void yybegin(int newState) {
    zzLexicalState = newState;
  }


  /**
   * Returns the text matched by the current regular expression.
   *
   * @return the matched text.
   */
  public final String yytext() {
    return new String(zzBuffer, zzStartRead, zzMarkedPos-zzStartRead);
  }


  /**
   * Returns the character at the given position from the matched text.
   *
   * <p>It is equivalent to {@code yytext().charAt(pos)}, but faster.
   *
   * @param position the position of the character to fetch. A value from 0 to {@code yylength()-1}.
   *
   * @return the character at {@code position}.
   */
  public final char yycharat(int position) {
    return zzBuffer[zzStartRead + position];
  }


  /**
   * How many characters were matched.
   *
   * @return the length of the matched text region.
   */
  public final int yylength() {
    return zzMarkedPos-zzStartRead;
  }


  /**
   * Reports an error that occurred while scanning.
   *
   * <p>In a well-formed scanner (no or only correct usage of {@code yypushback(int)} and a
   * match-all fallback rule) this method will only be called with things that
   * "Can't Possibly Happen".
   *
   * <p>If this method is called, something is seriously wrong (e.g. a JFlex bug producing a faulty
   * scanner etc.).
   *
   * <p>Usual syntax/scanner level error handling should be done in error fallback rules.
   *
   * @param errorCode the code of the error message to display.
   */
  private static void zzScanError(int errorCode) {
    String message;
    try {
      message = ZZ_ERROR_MSG[errorCode];
    } catch (ArrayIndexOutOfBoundsException e) {
      message = ZZ_ERROR_MSG[ZZ_UNKNOWN_ERROR];
    }

    throw new Error(message);
  }


  /**
   * Pushes the specified amount of characters back into the input stream.
   *
   * <p>They will be read again by then next call of the scanning method.
   *
   * @param number the number of characters to be read again. This number must not be greater than
   *     {@link #yylength()}.
   */
  public void yypushback(int number)  {
    if ( number > yylength() )
      zzScanError(ZZ_PUSHBACK_2BIG);

    zzMarkedPos -= number;
  }




  /**
   * Resumes scanning until the next regular expression is matched, the end of input is encountered
   * or an I/O-Error occurs.
   *
   * @return the next token.
   * @exception java.io.IOException if any I/O-Error occurs.
   */
  public int yylex() throws java.io.IOException
  {
    int zzInput;
    int zzAction;

    // cached fields:
    int zzCurrentPosL;
    int zzMarkedPosL;
    int zzEndReadL = zzEndRead;
    char[] zzBufferL = zzBuffer;

    int [] zzTransL = ZZ_TRANS;
    int [] zzRowMapL = ZZ_ROWMAP;
    int [] zzAttrL = ZZ_ATTRIBUTE;

    while (true) {
      zzMarkedPosL = zzMarkedPos;

      yychar+= zzMarkedPosL-zzStartRead;

      boolean zzR = false;
      int zzCh;
      int zzCharCount;
      for (zzCurrentPosL = zzStartRead  ;
           zzCurrentPosL < zzMarkedPosL ;
           zzCurrentPosL += zzCharCount ) {
        zzCh = Character.codePointAt(zzBufferL, zzCurrentPosL, zzMarkedPosL);
        zzCharCount = Character.charCount(zzCh);
        switch (zzCh) {
        case '\u000B':  // fall through
        case '\u000C':  // fall through
        case '\u0085':  // fall through
        case '\u2028':  // fall through
        case '\u2029':
          yyline++;
          yycolumn = 0;
          zzR = false;
          break;
        case '\r':
          yyline++;
          yycolumn = 0;
          zzR = true;
          break;
        case '\n':
          if (zzR)
            zzR = false;
          else {
            yyline++;
            yycolumn = 0;
          }
          break;
        default:
          zzR = false;
          yycolumn += zzCharCount;
        }
      }

      if (zzR) {
        // peek one character ahead if it is
        // (if we have counted one line too much)
        boolean zzPeek;
        if (zzMarkedPosL < zzEndReadL)
          zzPeek = zzBufferL[zzMarkedPosL] == '\n';
        else if (zzAtEOF)
          zzPeek = false;
        else {
          boolean eof = zzRefill();
          zzEndReadL = zzEndRead;
          zzMarkedPosL = zzMarkedPos;
          zzBufferL = zzBuffer;
          if (eof)
            zzPeek = false;
          else
            zzPeek = zzBufferL[zzMarkedPosL] == '\n';
        }
        if (zzPeek) yyline--;
      }
      zzAction = -1;

      zzCurrentPosL = zzCurrentPos = zzStartRead = zzMarkedPosL;

      zzState = ZZ_LEXSTATE[zzLexicalState];

      // set up zzAction for empty match case:
      int zzAttributes = zzAttrL[zzState];
      if ( (zzAttributes & 1) == 1 ) {
        zzAction = zzState;
      }


      zzForAction: {
        while (true) {

          if (zzCurrentPosL < zzEndReadL) {
            zzInput = Character.codePointAt(zzBufferL, zzCurrentPosL, zzEndReadL);
            zzCurrentPosL += Character.charCount(zzInput);
          }
          else if (zzAtEOF) {
            zzInput = YYEOF;
            break zzForAction;
          }
          else {
            // store back cached positions
            zzCurrentPos  = zzCurrentPosL;
            zzMarkedPos   = zzMarkedPosL;
            boolean eof = zzRefill();
            // get translated positions and possibly new buffer
            zzCurrentPosL  = zzCurrentPos;
            zzMarkedPosL   = zzMarkedPos;
            zzBufferL      = zzBuffer;
            zzEndReadL     = zzEndRead;
            if (eof) {
              zzInput = YYEOF;
              break zzForAction;
            }
            else {
              zzInput = Character.codePointAt(zzBufferL, zzCurrentPosL, zzEndReadL);
              zzCurrentPosL += Character.charCount(zzInput);
            }
          }
          int zzNext = zzTransL[ zzRowMapL[zzState] + zzCMap(zzInput) ];
          if (zzNext == -1) break zzForAction;
          zzState = zzNext;

          zzAttributes = zzAttrL[zzState];
          if ( (zzAttributes & 1) == 1 ) {
            zzAction = zzState;
            zzMarkedPosL = zzCurrentPosL;
            if ( (zzAttributes & 8) == 8 ) break zzForAction;
          }

        }
      }

      // store back cached position
      zzMarkedPos = zzMarkedPosL;

      if (zzInput == YYEOF && zzStartRead == zzCurrentPos) {
        zzAtEOF = true;
        return YYEOF;
      }
      else {
        switch (zzAction < 0 ? zzAction : ZZ_ACTION[zzAction]) {
          case 1:
            { //System.out.println("PreGAZZETTA: '" + yytext() + "'");
	output.append(yytext());
            }
          // fall through
          case 8: break;
          case 2:
            { output.append(yytext());
            }
          // fall through
          case 9: break;
          case 3:
            { superPartitions.append(yytext());
            }
          // fall through
          case 10: break;
          case 4:
            { if(readRubrica) {
		
			readRubrica = false;
			superPartitions.append(yytext());
			
		} else {
		
			yypushback(yylength());
			yybegin(0);
		}
            }
          // fall through
          case 11: break;
          case 5:
            { art1found = true;
	
	//System.out.println("PRE - ART_1 found");
	
	if(superPartitions != null) output.append(superPartitions.toString());
	
	output.append(yytext());
	
	superPartitions = null;
            }
          // fall through
          case 12: break;
          case 6:
            { //System.out.println("PRE - FIRST found: " + yytext());
	
	if(art1found || superPartitions == null) {
		
		output.append(yytext());
	
	} else {
		
		yypushback(yylength());
		yybegin(inSuper);
	}
            }
          // fall through
          case 13: break;
          case 7:
            { readRubrica = true;
		superPartitions.append(yytext());
            }
          // fall through
          case 14: break;
          default:
            zzScanError(ZZ_NO_MATCH);
        }
      }
    }
  }

  /**
   * Runs the scanner on input files.
   *
   * This is a standalone scanner, it will print any unmatched
   * text to System.out unchanged.
   *
   * @param argv   the command line, contains the filenames to run
   *               the scanner on.
   */
  public static void main(String[] argv) {
    if (argv.length == 0) {
      System.out.println("Usage : java PreGazzettaSuper [ --encoding <name> ] <inputfile(s)>");
    }
    else {
      int firstFilePos = 0;
      String encodingName = "UTF-8";
      if (argv[0].equals("--encoding")) {
        firstFilePos = 2;
        encodingName = argv[1];
        try {
          // Side-effect: is encodingName valid?
          java.nio.charset.Charset.forName(encodingName);
        } catch (Exception e) {
          System.out.println("Invalid encoding '" + encodingName + "'");
          return;
        }
      }
      for (int i = firstFilePos; i < argv.length; i++) {
        PreGazzettaSuper scanner = null;
        java.io.FileInputStream stream = null;
        java.io.Reader reader = null;
        try {
          stream = new java.io.FileInputStream(argv[i]);
          reader = new java.io.InputStreamReader(stream, encodingName);
          scanner = new PreGazzettaSuper(reader);
          while ( !scanner.zzAtEOF ) scanner.yylex();
        }
        catch (java.io.FileNotFoundException e) {
          System.out.println("File not found : \""+argv[i]+"\"");
        }
        catch (java.io.IOException e) {
          System.out.println("IO error scanning file \""+argv[i]+"\"");
          System.out.println(e);
        }
        catch (Exception e) {
          System.out.println("Unexpected exception:");
          e.printStackTrace();
        }
        finally {
          if (reader != null) {
            try {
              reader.close();
            }
            catch (java.io.IOException e) {
              System.out.println("IO error closing file \""+argv[i]+"\"");
              System.out.println(e);
            }
          }
          if (stream != null) {
            try {
              stream.close();
            }
            catch (java.io.IOException e) {
              System.out.println("IO error closing file \""+argv[i]+"\"");
              System.out.println(e);
            }
          }
        }
      }
    }
  }


}
