/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.marker;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Run {

	public static void main(String[] args) {
		
		/* Very basic Run class for testing the library from the command-line */

		if(args.length < 1) {
			
			help();
			
			return;
		}

		String fileName = args[0].trim();
		
		String inputCharSet = null;
		
		if(args.length > 1) inputCharSet = args[1].trim();
		
		MarkerDocument markerDocument = MarkerDocumentFactory.createMarkerDocumentFromFile(fileName, inputCharSet);
		
		Marker.run(markerDocument);

		try {
			
			String xml = markerDocument.getXml();
			if(xml != null) Files.write(Paths.get(fileName + ".xml"), xml.getBytes());
			
			String html = markerDocument.getHtml();
			if(html != null) Files.write(Paths.get(fileName + ".html"), html.getBytes());
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if(markerDocument.getMessages().size() > 0) {
			
			System.out.println("\n### Messages ###\n");
			
			for(String msg : markerDocument.getMessages()) System.out.println("\t - " + msg);
		}
		

		if(markerDocument.getWarnings().size() > 0) {
			
			System.out.println("\n### Warnings ###\n");
			
			for(String error : markerDocument.getWarnings()) System.out.println("\t - " + error);
		}

		if(markerDocument.getErrors().size() > 0) {
			
			System.out.println("\n### Errors ###\n");
			
			for(String error : markerDocument.getErrors()) System.out.println("\t - " + error);
		}
		
		if( !markerDocument.getValidationError().equals("")) {
			
			System.out.println("\n### Validation error ###\n");
			
			System.out.println("\t - " + markerDocument.getValidationError());
		}
		

		if( !markerDocument.getNotWellFormattedError().equals("")) {
			
			System.out.println("\n### Testo XML non ben formattato ###\n" + "\t - " + markerDocument.getNotWellFormattedError());
			
		}
		
		System.out.println("\nDone.");
	}
	
	private static void help() {
		
		System.out.println("Examples:\n");
		System.out.println("Run fileName");
		System.out.println("Run fileName charSet");
	}

}
