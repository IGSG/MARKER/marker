/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.marker;

public class Marker {

	final public static String VERSION = "0.9.1";
	final private static String NAME = "IGSG Marker";
	
	final public static String getInfo() { return NAME + " " + VERSION; }

	public static void run(MarkerDocument markerDocument) {
		
		if(markerDocument.getInputType() != null && markerDocument.getInputType().equals(InputType.XML_IGSG_MARKER)) {
			
			markerDocument.buildXml();
			
			markerDocument.buildHtml();
			
			return;
		}
		
		markerDocument.setExecutionTimeStart(System.currentTimeMillis());
		
		String currentText = markerDocument.getMarkerText();
		
		//System.out.println("\n\nMARKER TEXT :\n\n" + currentText);
		
		currentText = runService(markerDocument, "skip", currentText);
		
		if( !markerDocument.isOnlySubStructure()) {
		
			currentText = runService(markerDocument, "splitdoc", currentText);
			
			currentText = runService(markerDocument, "readdoc", currentText);
			
			if( !markerDocument.isArticolato()) {
				
				markerDocument.addMessage("Articolato non rilevato.");
			}
			
			currentText = runService(markerDocument, "post", currentText);
			
			if(markerDocument.isAllAmendmentsEnabled()) {
				
				currentText = runService(markerDocument, "modifiche", currentText);
			}
			
			runCheckIDs(markerDocument, currentText);
		}
		
		if(markerDocument.getInputType().equals(InputType.PLAIN_GAZZETTA_UFFICIALE) && markerDocument.isCheckApostropheEnabled()) {
			
			currentText = runCheckApostrophe(markerDocument, currentText);
		}
		
		markerDocument.setExecutionTimeEnd(System.currentTimeMillis());
		
		markerDocument.setXml(currentText);
	}
	
	static String runBody(MarkerDocument markerDocument, String text) {	
		
		CheckArticolato ca = new CheckArticolato();
		ca.setInput(text);
		ca.run();
		String art = ca.getOutput();
		
		if(art == null || art.equals("")) {
			
			if(markerDocument.isDebug()) System.out.println("CHECK ART: no articolato.");
			return "";
		}
		
		if(markerDocument.isDebug()) System.out.println("CHECK ART: " + art);
		
		String currentText = runService(markerDocument, "splitbody", text);
		
		currentText = runService(markerDocument, "readbody", currentText);
		
		return currentText;
	}

	static String runArticolato(MarkerDocument markerDocument, String text) {

		/*
		 * Spostato il servizio CheckNumberedItems all'inizio dell'articolato e non
		 * all'interno delle singole partizioni, in modo da agire anche all'interno
		 * di virgolette (altrimenti avviene chiusura forzata virgolette).
		 * 
		 * Commentato successivamente.
		 */
		
		text = Util.checkNumberedItems(markerDocument, text);
		
		//System.out.println("\n\nRUN ARTICOLATO ON:\n\n" + text);

		String currentText = runService(markerDocument, "virgolette", text);
		
		markerDocument.getContext().resetAll();
		markerDocument.getContext().setInQuotes(false);
		markerDocument.getContext().resetQuotes();

		currentText = runService(markerDocument, "libro", currentText);

		currentText = runService(markerDocument, "parte", currentText);

		currentText = runService(markerDocument, "titolo", currentText);

		currentText = runService(markerDocument, "capo", currentText);

		currentText = runService(markerDocument, "sezione", currentText);

		currentText = runService(markerDocument, "articolo", currentText);
		
		//Non aggiornare il currentText per il check degli articoli
		runService(markerDocument, "checkarticolo", currentText);
		
		//Non aggiornare il currentText per il check primo comma
		runService(markerDocument, "checkfirstcomma", currentText);
		
		//System.out.println("\n\nSTEP:\n\n" + currentText);
		
		//currentText = runService(markerDocument, "checknumlist", currentText);
		
		currentText = runService(markerDocument, "comma", currentText);
		
		currentText = runService(markerDocument, "paragrafo", currentText);
		
		if(markerDocument.isAllAmendmentsEnabled()) {
			
			currentText = runService(markerDocument, "parole", currentText);
		}
		
		currentText = runService(markerDocument, "lista", currentText);
		
		if(markerDocument.isArticlesTitlesEnabled()) {
			
			currentText = runService(markerDocument, "rubrica", currentText);
		}
		
		currentText = runService(markerDocument, "readquote", currentText);
		
		currentText = runService(markerDocument, "postbody", currentText);

		return currentText;
	}

	static String runVirgolette(MarkerDocument markerDocument, String text) {

		if(markerDocument.isDebug()) System.out.println("Virgolette " + markerDocument.getContext().getQuotesBaseId());
		
		if(markerDocument.isDebug()) System.out.println("\nRUN VIRGOLETTE ON: \n" + text);
		
		String textBeforeSuperPart = text;
		
		String currentText = runService(markerDocument, "titolo", text);
		
		currentText = runService(markerDocument, "capo", currentText);
		
		currentText = runService(markerDocument, "sezione", currentText);
		
		String textAfterSuperPart = currentText;
		
		String textBeforeArticle = currentText;
		
		currentText = runService(markerDocument, "articolo", currentText);
		
		String textAfterArticle = currentText;
		
		if( !textBeforeSuperPart.equals(textAfterSuperPart) && textBeforeArticle.equals(textAfterArticle)) {
			
			//Se sono presenti partizioni superiori all'articolo deve essere presente l'articolo per poter cercare partizioni inferiori
			
			//Non analizzare ulteriormente in questo caso
			
		} else {
		
			//Se è presente l'articolo plainQuotes=false
			if( !textBeforeArticle.equals(textAfterArticle)) markerDocument.getContext().setPlainQuotes(false);
	
			//if( !markerDocument.getContext().isPlainQuotes()) currentText = runService(markerDocument, "checknumlist", currentText);
			
			String textBeforeComma = currentText;
			currentText = runService(markerDocument, "comma", currentText);
			String textAfterComma = currentText;
			
			//Se è presente il comma plainQuotes=false
			if( !textBeforeComma.equals(textAfterComma)) markerDocument.getContext().setPlainQuotes(false);
			
			if( !markerDocument.getContext().isPlainQuotes()) {
				
				if(markerDocument.isDebug()) System.out.println("plainQuotes is false");
				
				//Identifica i paragrafi solo se sono stati già individuati articoli o commi nelle virgolette
				
				currentText = runService(markerDocument, "paragrafo", currentText);
			}
			
			currentText = runService(markerDocument, "lista", currentText);
		}
		
		currentText = runService(markerDocument, "rubrica", currentText);
		
		return currentText;
	}
	
	private static void runCheckIDs(MarkerDocument markerDocument, String text) {
		
		CheckIDs check = new CheckIDs();
		check.setMarkerDocument(markerDocument);
		check.setInput(text);
		check.run();
	}

	private static String runCheckApostrophe(MarkerDocument markerDocument, String text) {
		
		CheckApostrophe checkApostrophe = new CheckApostrophe();
		checkApostrophe.setInput(text);
		checkApostrophe.setMarkerDocument(markerDocument);
		checkApostrophe.run();
		
		return checkApostrophe.getOutput();
	}

	private static String runService(MarkerDocument markerDocument, String serviceName, String text) {
		
		if(text == null) return "";
		
		if(markerDocument.isDebug()) System.out.println(serviceName + "...");
		
		JFlexAnnotationService service = null;
		
		if(serviceName.equalsIgnoreCase("splitdoc")) service = new SplitDocuments();
		if(serviceName.equalsIgnoreCase("readdoc")) service = new ReadDocument();
		if(serviceName.equalsIgnoreCase("checkart")) service = new CheckArticolato();
		if(serviceName.equalsIgnoreCase("splitbody")) service = new SplitBodies();
		if(serviceName.equalsIgnoreCase("readbody")) service = new ReadBody();
		if(serviceName.equalsIgnoreCase("skip")) service = new MrkSkipTable();
		if(serviceName.equalsIgnoreCase("virgolette")) service = new Quotes();
		if(serviceName.equalsIgnoreCase("libro")) service = new ReadSuperPart();
		if(serviceName.equalsIgnoreCase("parte")) service = new ReadSuperPart();
		if(serviceName.equalsIgnoreCase("titolo")) service = new ReadSuperPart();
		if(serviceName.equalsIgnoreCase("capo")) service = new ReadSuperPart();
		if(serviceName.equalsIgnoreCase("sezione")) service = new ReadSuperPart();
		if(serviceName.equalsIgnoreCase("articolo")) service = new ReadSuperPart();
		if(serviceName.equalsIgnoreCase("checkarticolo")) service = new ReadPart();
		if(serviceName.equalsIgnoreCase("checkfirstcomma")) service = new ReadPart();
		if(serviceName.equalsIgnoreCase("checknumlist")) service = new ReadPart();
		if(serviceName.equalsIgnoreCase("comma")) service = new ReadPart();
		if(serviceName.equalsIgnoreCase("paragrafo")) service = new ReadPart();
		if(serviceName.equalsIgnoreCase("lista")) service = new ReadPart();
		
		if(serviceName.equalsIgnoreCase("rubrica")) service = new Titles();
		
		if(serviceName.equalsIgnoreCase("readquote")) service = new ReadQuotes();
		if(serviceName.equalsIgnoreCase("postbody")) service = new PostBody();
		if(serviceName.equalsIgnoreCase("post")) service = new Post();
		if(serviceName.equalsIgnoreCase("parole")) service = new ModificheParole();
		if(serviceName.equalsIgnoreCase("modifiche")) service = new Modifiche();

		if(service == null) {
			
			markerDocument.addError("Unknown service name: " + serviceName);
			
			return "";
		}
		
		markerDocument.getContext().setCurrentPartType(serviceName);
			
		service.setMarkerDocument(markerDocument);
		service.setInput(text);
		service.run();
		
		if(serviceName.equalsIgnoreCase("splitdoc")) {
			
			if(((SplitDocuments) service).getGuardasigilli().equals("")) {
				
				if(markerDocument.isDebug()) System.out.println("Guardasigilli not found - running second pass...");
				
				service = new SplitDocuments();
				service.setMarkerDocument(markerDocument);
				service.setInput(text);
				((SplitDocuments) service).setSecondPass(true);
				service.run();
			}
		}

		String output = service.getOutput();

		/*
		//E' inutile togliere postbody dai controlli perchè c'è anche il controllo readbody sul suo contenuto
		if( !serviceName.equals("checkarticolo") && !serviceName.equals("post") && output != null && !checkAnnotations(text, output, serviceName, markerDocument.isDebug())) {
						
			markerDocument.addError("controllo di consistenza del testo fallito nel servizio \"" + serviceName + "\" (inQuotes:" + markerDocument.getContext().isInQuotes() + ")");
			
			return "";
		}
		*/

		//Non controllare i macro-servizi (i controlli avvengono nei servizi interni)
		if( !serviceName.equals("skip") && !serviceName.equals("readdoc") && !serviceName.equals("readbody") && !serviceName.equals("readquote") && !serviceName.equals("checkarticolo") 
				&& !serviceName.equals("post") && !serviceName.equals("checknumlist") && !serviceName.equals("checkfirstcomma") && output != null && !checkAnnotations(text, output, serviceName, markerDocument.isDebug())) {
			
			String quotesBaseId = "";
			if(markerDocument.getContext().isInQuotes()) {
				
				quotesBaseId = markerDocument.getContext().getQuotesBaseId();
				
				//Togli il suffisso _virgolette_ per avere un puntatore corretto nell'idref
				//es.: articolo-1_lettera-b_numero-2_virgolette_
				if(quotesBaseId.endsWith("_virgolette_")) {
					
					quotesBaseId = quotesBaseId.substring(0, quotesBaseId.length() - "_virgolette_".length());
				}
				
				quotesBaseId = "idref:" + quotesBaseId + " ";

			}
			
			markerDocument.addError(quotesBaseId + "controllo di consistenza del testo fallito nel servizio \"" + serviceName + "\" (inQuotes:" + markerDocument.getContext().isInQuotes() + ")");
			
			if(serviceName.equals("rubrica")) {
				
				return text;
			}
			
			return "";
		}

		return output;
	}
	
	static boolean checkAnnotations(String beforeXml, String afterXml, String serviceName, boolean debug) {
		
		String before = Util.getOnlyMeaningfulText(beforeXml);
		String after = Util.getOnlyMeaningfulText(afterXml);
		
		if( !after.equals(before)) {
			
			if(debug) System.out.println("\n\nCheck text failed (" + serviceName + ").");
			if(debug) System.out.println("\n\nBEFORE:\n\n<<<\n" + beforeXml + "\n>>>");
			if(debug) System.out.println("\n\nAFTER:\n\n<<<\n" + afterXml + "\n>>>");
			if(debug) System.out.println("\n\nCheck text failed. Equal part:\n" + Util.getEqualPart(after, before) + "\n\n");
			
			return false;
		}

		return true;
	}	
}
