/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.marker;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

public class MarkerDocumentFactory {

	public static MarkerDocument createMarkerDocument(String text) {
		
		return createMarkerDocument(text, null);
	}

	public static MarkerDocument createMarkerDocument(String text, InputType inputType) {
		
		MarkerDocument markerDocument = new MarkerDocument();
		
		markerDocument.setInputType(inputType);

		markerDocument.setText(text);
		
		return markerDocument;
	}

	/**
	 * Default inputType: PLAIN_GAZZETTA_UFFICIALE
	 * 
	 * @param fileName
	 * @return
	 */
	public static MarkerDocument createMarkerDocumentFromFile(String fileName) {
		
		return createMarkerDocumentFromFile(fileName, null, null);
	}
	
	public static MarkerDocument createMarkerDocumentFromFile(String fileName, InputType inputType) {
		
		return createMarkerDocumentFromFile(fileName, inputType, null);
	}
	
	/**
	 * Default inputType: PLAIN_GAZZETTA_UFFICIALE
	 * 
	 * @param fileName
	 * @param inputCharSet
	 * @return
	 */
	public static MarkerDocument createMarkerDocumentFromFile(String fileName, String inputCharSet) {
		
		return createMarkerDocumentFromFile(fileName, null, inputCharSet);
	}
	
	public static MarkerDocument createMarkerDocumentFromFile(String fileName, InputType inputType, String inputCharSet) {

		String text = null;
		
		inputCharSet = Util.normalizeCharSet(inputCharSet);
		
		try {

			if(inputCharSet == null) {

				//Utilizza il charset di default dell'environment
				text = new String(Files.readAllBytes(Paths.get(fileName)));
			
			} else {
				
				//Java 8
				text = new String(Files.readAllBytes(Paths.get(fileName)), inputCharSet);
			}
			
		} catch (IOException e) {
			
			System.err.println("File error or not found: " + fileName);
			
			return null;
		}
		
		MarkerDocument markerDocument = new MarkerDocument();
		
		markerDocument.setInputType(inputType);

		markerDocument.setFileName(fileName);
		
		markerDocument.setText(text);

		/*
		 * Imposta come encoding per l'output la rappresentazione interna utilizzata a runtime.
		 * In un sistema windows sarà probabilmente windows-1252: in quel caso per ottenere
		 * un output di tipo UTF-8 dovrà essere lanciata l'applicazione con il paramentro
		 * java -Dfile.encoding=UTF-8
		 * che forza la rappresentazione interna ad essere UTF-8.
		 */
		markerDocument.setOutputEncoding(Util.normalizeCharSet(Charset.defaultCharset().toString()));
		
		return markerDocument;
	}
}
