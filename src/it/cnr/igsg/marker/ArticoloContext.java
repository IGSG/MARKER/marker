/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.marker;

public class ArticoloContext {

	/*
	 * 
	 * Contesto dell'articolo da utilizzare per effettuare dei "guess" sulla rubrica,
	 * non per comandare strettamente l'analisi dell'articolo e del suo contenuto.
	 * 
	 */
	
	private String id = "";
	
	//Se è stata individuata nei primi paragrafi la dichiarazione di primo comma
	private boolean commiNumerati = false;
	
	//Se non è presente testo prima dell'inizio del primo comma
	private boolean nothingBeforeComma = false;
	
	//Se il testo del primo paragrafo inizia con una parentesi (
	private boolean firstParagraphInBrackets = false;

	//Se il testo del primo paragrafo termina con due punti oppure contiene virgolette, tabelle o pre
	private boolean firstNotRubrica = false;

	//Se è presente un solo paragrafo con testo significativo (non vuoto)
	private boolean oneMeaningfulParagraph = false;

	//Da considerare in caso di first meaningful paragraph ambiguo
	private int firstParagraphLength = 0;

	String getId() {
		return id;
	}

	void setId(String id) {
		this.id = id;
	}

	boolean isCommiNumerati() {
		return commiNumerati;
	}

	void setCommiNumerati(boolean commiNumerati) {
		this.commiNumerati = commiNumerati;
	}

	boolean isFirstParagraphInBrackets() {
		return firstParagraphInBrackets;
	}

	void setFirstParagraphInBrackets(boolean firstParagraphInBrackets) {
		this.firstParagraphInBrackets = firstParagraphInBrackets;
	}

	boolean isOneMeaningfulParagraph() {
		return oneMeaningfulParagraph;
	}

	void setOneMeaningfulParagraph(boolean oneMeaningfulParagraph) {
		this.oneMeaningfulParagraph = oneMeaningfulParagraph;
	}

	boolean isFirstNotRubrica() {
		return firstNotRubrica;
	}

	void setFirstNotRubrica(boolean firstNotRubrica) {
		this.firstNotRubrica = firstNotRubrica;
	}

	boolean isNothingBeforeComma() {
		return nothingBeforeComma;
	}

	void setNothingBeforeComma(boolean nothingBeforeComma) {
		this.nothingBeforeComma = nothingBeforeComma;
	}

	int getFirstParagraphLength() {
		return firstParagraphLength;
	}

	void setFirstParagraphLength(int firstParagraphLength) {
		this.firstParagraphLength = firstParagraphLength;
	}
}
