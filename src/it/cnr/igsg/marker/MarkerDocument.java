/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.marker;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.w3c.dom.Document;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class MarkerDocument {

	final static private String XSLT_FILENAME = "xsl/marker.xsl";	
	final static private String XSD_FILENAME = "xsd/marker.xsd";

	static private Validator validator = null;
	

	//Configurazione per aquisizione del testo, per la generazione dell'html e per il run di Marker

	//default Gazzetta Ufficiale - must never be null
	private InputType inputType = InputType.PLAIN_GAZZETTA_UFFICIALE;


	private boolean validationEnabled = true;

	private boolean headerEnabled = true;
	
	private boolean annexEnabled = true;
	
	private boolean articlesTitlesEnabled = true;
	
	private boolean checkApostropheEnabled = true;
	
	private boolean allAmendmentsEnabled = true;
	
	private boolean onlySubStructure = false;
	
	private boolean debug = false;
	
	
	//Custom - TODO remove
	private boolean extendEuTitles = true;

	
	
	InputType getInputType() {
		return inputType;
	}

	void setInputType(InputType inputType) {
		
		if(inputType != null) this.inputType = inputType;
	}

	boolean isValidationEnabled() {
		return validationEnabled;
	}

	/**
	 * Abilita o disabilita la validazione dell'output XML
	 * (default: true)
	 * 
	 * @param validationEnabled
	 */
	public void setValidationEnabled(boolean validationEnabled) {
		this.validationEnabled = validationEnabled;
	}

	boolean isHeaderEnabled() {
		return headerEnabled;
	}

	/**
	 * Abilita o disabilita l'analisi della testa del documento
	 * (default: true)
	 * 
	 * @param headerEnabled
	 */
	public void setHeaderEnabled(boolean headerEnabled) {
		this.headerEnabled = headerEnabled;
	}

	boolean isAnnexEnabled() {
		return annexEnabled;
	}

	/**
	 * Abilita o disabilita l'analisi degli allegati
	 * (default: true)
	 * 
	 * @param annexEnabled
	 */
	public void setAnnexEnabled(boolean annexEnabled) {
		this.annexEnabled = annexEnabled;
	}

	boolean isArticlesTitlesEnabled() {
		return articlesTitlesEnabled;
	}

	/**
	 * Abilita o disabilita l'annotazione delle rubriche degli articoli
	 * (default: true)
	 * 
	 * @param articlesTitlesEnabled
	 */
	public void setArticlesTitlesEnabled(boolean articlesTitlesEnabled) {
		this.articlesTitlesEnabled = articlesTitlesEnabled;
	}

	boolean isOnlySubStructure() {
		return onlySubStructure;
	}

	/**
	 * Indica se eseguire soltanto la marcatura della 
	 * sotto-struttura (composta di soli tag &lt;p&gt; &lt;br&gt; &lt;table&gt; e &lt;pre&gt;)
	 * (default: false)
	 * 
	 * @param onlySubStructure
	 */
	public void setOnlySubStructure(boolean onlySubStructure) {
		this.onlySubStructure = onlySubStructure;
	}

	boolean isCheckApostropheEnabled() {
		return checkApostropheEnabled;
	}
	
	/**
	 * Abilita o disabilita la funzione di trasformazione delle parole 
	 * da apostrofate ad accentate (solo se fonte Gazzetta Ufficiale).
	 * (default: true)
	 * 
	 * @param checkApostropheEnabled
	 */
	public void setCheckApostropheEnabled(boolean checkApostropheEnabled) {
		this.checkApostropheEnabled = checkApostropheEnabled;
	}

	/**
	 * Indica se è attivata l'annotazione delle modifiche di parola.
	 * (default: true)
	 * @return
	 */
	public boolean isAllAmendmentsEnabled() {
		return allAmendmentsEnabled;
	}

	/**
	 * Abilita o disabilita l'annotazione delle modifiche di parola.
	 * @param allAmendmentsEnabled
	 */
	public void setAllAmendmentsEnabled(boolean allAmendmentsEnabled) {
		this.allAmendmentsEnabled = allAmendmentsEnabled;
	}

	boolean isExtendEuTitles() {
		return extendEuTitles;
	}

	public void setExtendEuTitles(boolean extendEuTitles) {
		this.extendEuTitles = extendEuTitles;
	}

	boolean isDebug() {
		return debug;
	}

	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	private String outputEncoding = null;
	
	void setOutputEncoding(String outputEncoding) {
		this.outputEncoding = outputEncoding;
	}

	MarkerDocument() {
		
		init();
	}
	
	private void init() {
		
		context = new Context();
		context.setMarkerDocument(this);
		
		errors = new ArrayList<String>();
		warnings = new ArrayList<String>();
		messages = new ArrayList<String>();
	}
	
	private long executionTime = 0;
	private long executionTimeStart = 0;
	private long executionTimeEnd = 0;
	
	void setExecutionTimeStart(long executionTimeStart) {
		this.executionTimeStart = executionTimeStart;
	}

	void setExecutionTimeEnd(long executionTimeEnd) {
		this.executionTimeEnd = executionTimeEnd;
		this.executionTime += this.executionTimeEnd - this.executionTimeStart;
	}
	
	long getExecutionTime() { return this.executionTime; }

	private Context context = null;
	
	private String fileName = "";

	void setFileName(String fileName) {
		this.fileName = fileName;
	}

	private String originalText = "";
	
	//Il testo dopo il pre-processing ed il table e paragraph identification
	private String markerText = "";

	
	//Marker XML main output
	private String xml = "";
	
	//HTML per mostrare i risultati della marcatura prodotto con un foglio di stile
	private String html = "";
	
	public String getHtml() {
		
		return this.html;
	}
	
	private ArrayList<String> messages = new ArrayList<String>();
	private ArrayList<String> warnings = new ArrayList<String>();
	private ArrayList<String> errors = new ArrayList<String>();
	

	public List<String> getMessages() {
		
		return Collections.unmodifiableList(messages);
	}

	void addMessage(String message) {
		
		this.messages.add(message);
	}

	public List<String> getWarnings() {
		
		return Collections.unmodifiableList(warnings);
	}

	void addWarning(String warning) {
		
		this.warnings.add(warning);
	}

	public List<String> getErrors() {
		
		return Collections.unmodifiableList(errors);
	}

	void addError(String error) {
		
		this.errors.add(error);
	}
	
	private String validationError = "";
	private String notWellFormattedError = "";
	private boolean notWellFormattedTables = false;
	
	void setNotWellFormattedTables(boolean notWellFormattedTables) {
		this.notWellFormattedTables = notWellFormattedTables;
	}

	public String getValidationError() {
		return validationError;
	}

	public String getNotWellFormattedError() {
		return notWellFormattedError;
	}

	private String eli = "";
	
	void setEli(String eli) {
		this.eli = eli;
	}

	private String vigenza = "";
	
	void setVigenza(String vigenza) {
		
		this.vigenza = vigenza;
	}

	private boolean articolato = false;
	
	public boolean isArticolato() {
		return articolato;
	}

	void setArticolato(boolean articolato) {
		this.articolato = articolato;
	}

	String getOriginalText() {
		return originalText;
	}
	
	Context getContext() {
		return context;
	}

	String getMarkerText() {
		return markerText;
	}

	void setText(String inputText) {
		
		executionTimeStart = System.currentTimeMillis();
		
		this.originalText = inputText;
		
		//Pre processing
		
		if(inputType.equals(InputType.XML_IGSG_MARKER)) {
			
			PreXml px = new PreXml();
			
			px.setInput(inputText);
			px.run();
			
			this.xml = px.getOutput();
			
			return;
		}
		
		if(isDebug()) System.out.println("pre...");
		
		Pre pre = new Pre();
		
		pre.setMarkerDocument(this);
		pre.setInput(originalText);
		pre.run();
		
		String currentText = pre.getOutput();
		
		if(inputType.equals(InputType.PLAIN_GAZZETTA_UFFICIALE)) {
			
			PreGazzetta preGazzetta = new PreGazzetta();
			
			preGazzetta.setInput(currentText);
			preGazzetta.run();
			
			currentText = preGazzetta.getOutput();
			
			ReadTable rt = new ReadTable();
			
			rt.setInput(currentText);
			rt.setMarkerDocument(this);
			rt.run();
			
			ParagraphGazzetta paragraphGazzetta = new ParagraphGazzetta();
			paragraphGazzetta.setInput(rt.getOutput());
			paragraphGazzetta.run();

			PreGazzettaSuper preGazzettaSuper = new PreGazzettaSuper();
			
			preGazzettaSuper.setInput(paragraphGazzetta.getOutput());
			preGazzettaSuper.run();
			
			String superText = preGazzettaSuper.getOutput();
			
			if(preGazzettaSuper.getSuperPartitions() != null && !preGazzettaSuper.getSuperPartitions().toString().equals("")) {
				
				if(isDebug()) System.out.println("ERROR IN PRE GAZZETTA SUPER - using original text");
				
				this.markerText = paragraphGazzetta.getOutput();
				
			} else {
				
				this.markerText = superText;
			}
			
		} else {
			
			PreEurLex preEurLex = new PreEurLex();
			
			preEurLex.setInput(currentText);
			preEurLex.run();
			
			currentText = preEurLex.getOutput();
			
			ParagraphEurLex paragraphEurLex = new ParagraphEurLex();
			paragraphEurLex.setInput(preEurLex.getOutput());
			paragraphEurLex.run();
			
			this.markerText = paragraphEurLex.getOutput();
		}
				
		this.executionTime = System.currentTimeMillis() - executionTimeStart;
		this.executionTimeStart = 0;
		this.executionTimeEnd = 0;
	}
	
	public String getXml() {
		
		return xml;
	}

	void setXml(String xml) {
		
		if(this.xml.equals("")) {
			
			Stats stats = new Stats();
			stats.setMarkerDocument(this);
			stats.setInput(xml);
			stats.run();
			
			String report = "Allegati identificati: " + stats.allegati;
			
			addMessage(report);
			
			report = "Partizioni totali identificate: ";

			if(stats.libri > 0) report += stats.libri + " libri; ";
			if(stats.parti > 0) report += stats.parti + " parti; ";
			if(stats.titoli > 0) report += stats.titoli + " titoli; ";
			if(stats.capi > 0) report += stats.capi + " capi; ";
			if(stats.sezioni > 0) report += stats.sezioni + " sezioni; ";
			if(stats.articoli > 0) report += stats.articoli + " articoli; ";
			if(stats.commi > 0) report += stats.commi + " commi; ";
			if(stats.paragrafi > 0) report += stats.paragrafi + " paragrafi; ";
			if(stats.lettere > 0) report += stats.lettere + " elementi di liste letterali; ";
			if(stats.numeri > 0) report += stats.numeri + " elementi di liste numeriche; ";
			if(stats.subnumeri > 0) report += stats.subnumeri + " elementi di liste sotto-numeriche; ";
			if(stats.latins > 0) report += stats.latins + " elementi di liste latine; ";
			
			if(stats.inserimenti > 0 || stats.soppressioni > 0) {
				
				addMessage(report);
				
				report = "Modifiche di parole identificate: Inserimenti: " + stats.inserimenti + " Soppressioni o sostituzioni: " + stats.soppressioni;
			}

			addMessage(report);
			
			report = "Virgolette identificate (modifiche di struttura): " + stats.virgolette;
			
			addMessage(report);
			
			if(stats.virgolette > 0) {

				report = "Partizioni identificate all'interno di virgolette: ";
	
				if(stats.vcapi > 0) report += stats.vcapi + " capi; ";
				if(stats.varticoli > 0) report += stats.varticoli + " articoli; ";
				if(stats.vcommi > 0) report += stats.vcommi + " commi; ";
				if(stats.vparagrafi > 0) report += stats.vparagrafi + " paragrafi; ";
				if(stats.vlettere > 0) report += stats.vlettere + " elementi di liste letterali; ";
				if(stats.vnumeri > 0) report += stats.vnumeri + " elementi di liste numeriche; ";
				if(stats.vsubnumeri > 0) report += stats.vsubnumeri + " elementi di liste sotto-numeriche; ";
				if(stats.vlatins > 0) report += stats.vlatins + " elementi di liste latine; ";
				
				addMessage(report);
			}
			
			//Questo encoding viene sovrascritto da prettyPrintXml(), settarlo comunque anche qua nel caso prettyPrint non venisse eseguito o fallisse
			
			String encoding = "UTF-8";
			
			if(outputEncoding != null) encoding = outputEncoding;
			
			this.xml =  "<?xml version=\"1.0\" encoding=\"" + encoding + "\"?>\n<root xmlns=\"https://www.igsg.cnr.it/marker/\" xmlns:h=\"http://www.w3.org/1999/xhtml\" xmlns:mrk=\"https://www.igsg.cnr.it/marker/\" xmlns:lkn=\"https://www.igsg.cnr.it/linkoln/\">";
			
			String metadata = "";

			metadata += "\n<mrk:metadata>\n<mrk:info>" + Marker.getInfo() + " [execution time: " + getExecutionTime() + "ms]</mrk:info>";
			
			//eli
			if( !this.eli.equals("")) {
				
				metadata += "\n<mrk:eli>" + this.eli + "</mrk:eli>";
			}
			
			//vigenza
			if( !this.vigenza.equals("")) {
				
				metadata += "\n<mrk:vigenza>" + this.vigenza + "</mrk:vigenza>";
			}
			
			//errors
			metadata += "\n<mrk:errors>";
			
			for(String error : errors) {
				
				String idref = "";
				
				if(error.startsWith("idref")) {
					
					idref = error.substring(error.indexOf(":")+1, error.indexOf(" "));
					
					error = error.substring(error.indexOf(" "));
				}
				
				if(idref.equals("")) {
					
					metadata += "\n<mrk:error>" + error + "</mrk:error>"; 

				} else {
					
					metadata += "\n<mrk:error idref=\"" + idref + "\">" + idref + ": " + error + "</mrk:error>"; 
				}
			}
			
			metadata += "\n</mrk:errors>";

			//warnings
			metadata += "\n<mrk:warnings>";
			
			if(this.notWellFormattedTables) {
				
				metadata += "\n<mrk:warning>Presenza di tabelle non ben formattate: annotate con tag &lt;h:pre&gt;</mrk:warning>";
			}
			
			for(String warning : warnings) {
				
				String idref = "";
				
				if(warning.startsWith("idref")) {
					
					idref = warning.substring(warning.indexOf(":")+1, warning.indexOf(" "));
					
					warning = warning.substring(warning.indexOf(" "));
				}
				
				if(idref.equals("")) {
					
					metadata += "\n<mrk:warning>" + warning + "</mrk:warning>"; 

				} else {
					
					metadata += "\n<mrk:warning idref=\"" + idref + "\">" + idref + ": " + warning + "</mrk:warning>"; 
				}
			}
			
			metadata += "\n</mrk:warnings>";

			//messages
			metadata += "\n<mrk:messages>";
			
			for(String message : messages) metadata += "\n<mrk:message>" + message + "</mrk:message>"; 
			
			metadata += "\n</mrk:messages>";
			
			metadata += "\n</mrk:metadata>\n";
			
			this.xml += metadata;
			
			this.xml += "\n" + xml + "\n";
			
			this.xml += "</root>\n";
			
			buildXml();
			
			buildHtml();
		}		
	}
	
	void buildXml() {
		
		try {

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);

			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(new InputSource(new StringReader(this.xml)));
			
			this.xml = prettyPrintXML(doc);

			if(isValidationEnabled()) {

				if(isDebug()) System.out.println("\nValidate..");

				if( !validate()) {
				
					errors.add("Output XML non valido");
				}
			}

		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SAXParseException e) {
			
			//Questo messaggio di errore deve essere trattato allo stesso modo del validation error e messo in fondo all'output XML
			
			String msg = e.getMessage();
			
			msg = msg.replaceAll("\\<", "&lt;");
			msg = msg.replaceAll("\\>", "&gt;");
			
			notWellFormattedError = "documento XML non ben formato! riga " + e.getLineNumber() + " colonna " + e.getColumnNumber() + ": " + msg + "";
			
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if( !this.validationError.equals("") || !this.notWellFormattedError.equals("")) {
			
			this.xml = this.xml.substring(0, this.xml.length() - "</root>\n".length());
			
			if( !this.validationError.equals("")) this.xml += "<mrk:validationError>" + this.validationError + "</mrk:validationError>\n";
			
			if( !this.notWellFormattedError.equals("")) this.xml += "<mrk:notWellFormattedError>" + notWellFormattedError + "</mrk:notWellFormattedError>\n";
			
			this.xml += "</root>";
		}		
	}
	
	private boolean validate() {

		if(validator == null) {
			
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			
			Schema schema = null;
			
			File xsdFile = Paths.get(XSD_FILENAME).toFile();

			try {
				
				if(xsdFile.exists()) {
					
					schema = schemaFactory.newSchema(xsdFile);
					
				} else {
					
					URL schemaURL = MarkerDocument.class.getResource("/" + XSD_FILENAME);
					
					if(schemaURL != null) schema = schemaFactory.newSchema(schemaURL);
				}
				
				if(schema == null) {
					
					String message = "validazione XML fallita: XSD schema not found!";
					
					this.validationError = message;
					
					return false;
				}
	
				validator = schema.newValidator();
			
			} catch (SAXException e) {
				
				System.err.println(e.getMessage());
				e.printStackTrace();
				
				return false;
			}
		}
		
		try {

			validator.validate(new StreamSource(new StringReader(this.xml)));

		} catch (SAXParseException e) {
			
			String message = "validazione XML fallita! riga " + e.getLineNumber() + " colonna " + e.getColumnNumber() + ": \"" + e.getMessage() + "\"";
			
			this.validationError = message;
			
		} catch (SAXException e) {
			
			if( !fileName.equals("")) System.err.println(fileName);
			System.err.println(e.getMessage());
			e.printStackTrace();
			
			return false;
			
		} catch (IOException e) {
			
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	private String prettyPrintXML(Document doc)  {

		try {
			
			DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
			DOMImplementationLS impl = (DOMImplementationLS)registry.getDOMImplementation("LS");

			LSSerializer writer = impl.createLSSerializer();
			writer.getDomConfig().setParameter("format-pretty-print", Boolean.TRUE);
			
			LSOutput lsOutput = impl.createLSOutput();
			
			if(outputEncoding != null) 
				lsOutput.setEncoding(outputEncoding);
			else
				lsOutput.setEncoding("UTF-8");
			
			//Forza UTF-8 in uscita, in caso di problemi su input non UTF-8 passare il parametro a Java: -Dfile.encoding=UTF-8
			//lsOutput.setEncoding("UTF-8");

			StringWriter stringWriter = new StringWriter();
		    lsOutput.setCharacterStream(stringWriter);
		    writer.write(doc, lsOutput);
		    
		    return stringWriter.toString();
			
		} catch(Exception e) {
			
			e.printStackTrace();
			
			return "";
		}
	}

	String buildHtml() {
		
		if(xml == null || xml.trim().length() < 1 || !notWellFormattedError.equals("")) return null;
		
		try {
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);

			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(new InputSource(new StringReader(xml)));
			if(isDebug()) System.out.println("\nTransform..");
			
			this.html = transform(doc);
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	private static String transform(Document doc) throws TransformerException {

		File xsltFile = Paths.get(XSLT_FILENAME).toFile();
       
		StringWriter outWriter = new StringWriter();

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		
		Transformer transformer = null;
		
		if(xsltFile.exists()) {
		
	        transformer = transformerFactory.newTransformer(new StreamSource(xsltFile));
	
		} else {
			
	        transformer = transformerFactory.newTransformer(new StreamSource(MarkerDocument.class.getResourceAsStream("/" + XSLT_FILENAME)));
		}
		
        transformer.transform(new DOMSource(doc), new StreamResult(outWriter));
        
        return outWriter.getBuffer().toString();
    }
}
