/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.marker;

import java.io.IOException;
import java.io.StringReader;

public abstract class JFlexAnnotationService {

	private MarkerDocument markerDocument = null;
	
	MarkerDocument getMarkerDocument() {
		return markerDocument;
	}

	void setMarkerDocument(MarkerDocument markerDocument) {
		
		this.markerDocument = markerDocument;
	}

	protected String input = "";
	
	void setInput(String input) {
		
		this.input = input;
	}

	protected StringBuilder output = new StringBuilder();
	
	String getOutput() {
	
		if(output == null) return null;
		
		return output.toString();
	}
	
	protected boolean first = true;

	protected String spaces = "";
	
	final boolean run() {
		
		output = new StringBuilder();
		first = true;
		spaces = "";
		
		try {
			
			yyreset(new StringReader(input));
			yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	abstract void yyreset(java.io.Reader reader);
	
	abstract int yylex() throws java.io.IOException;
	
}
