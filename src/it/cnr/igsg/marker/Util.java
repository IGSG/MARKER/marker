/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.marker;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.text.Normalizer;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util {

	private static final Pattern digits = Pattern.compile("\\d+");
	
	private final static TreeMap<Integer, String> romanNumerals = new TreeMap<Integer, String>();
	
	static {
		
		romanNumerals.put(1000, "M");
		romanNumerals.put(900, "CM");
		romanNumerals.put(500, "D");
		romanNumerals.put(400, "CD");
		romanNumerals.put(100, "C");
		romanNumerals.put(90, "XC");
		romanNumerals.put(50, "L");
        romanNumerals.put(40, "XL");
        romanNumerals.put(10, "X");
        romanNumerals.put(9, "IX");
        romanNumerals.put(5, "V");
        romanNumerals.put(4, "IV");
        romanNumerals.put(1, "I");
	}
	
	final static String getRomanNumeralFromInteger(int number) {
		
		int l =  romanNumerals.floorKey(number);
		
		if ( number == l ) {
			
			return romanNumerals.get(number);
		}
		
		return romanNumerals.get(l) + getRomanNumeralFromInteger(number-l);
    }
	
	private static Map<Character, Integer> romanNumeralsLookup = new LinkedHashMap<>();
	static {
		romanNumeralsLookup.put('I', 1);
		romanNumeralsLookup.put('V', 5);
		romanNumeralsLookup.put('X', 10);
		romanNumeralsLookup.put('L', 50);
		romanNumeralsLookup.put('C', 100);
		romanNumeralsLookup.put('D', 500);
		romanNumeralsLookup.put('M', 1000);
	}

	static final int getIntegerFromRomanNumeral(String s) {
		
		return getIntegerFromRomanNumeral(s, 0);
	}

	private static final int getIntegerFromRomanNumeral(String s, int index) {
		
		if(index == s.length()) {
			
			return 0;
		}
		
		return getRNumber(s, index) + getIntegerFromRomanNumeral(s.substring(index+1, s.length()));
	}

	private static final int getRNumber(String s, int index) {
		
		if(index+1 == s.length()) {
			
			return romanNumeralsLookup.get(s.charAt(index));
		}
		
		if (romanNumeralsLookup.get(s.charAt(index)) >= romanNumeralsLookup.get(s.charAt(index+1))) {
			
			return romanNumeralsLookup.get(s.charAt(index));
			
		} else {
			
			return -romanNumeralsLookup.get(s.charAt(index));
		}		
	}
	
	static String getRomanSuffixFromInteger(int n) {
		
		if(n == 1) return "semel";
		if(n == 2) return "bis";
		if(n == 3) return "ter";
		if(n == 4) return "quater";
		if(n == 5) return "quinquies";
		if(n == 6) return "sexies";
		if(n == 7) return "septies";
		if(n == 8) return "octies";
		if(n == 9) return "novies";
		if(n == 10) return "decies";
		if(n == 11) return "undecies";
		if(n == 12) return "duodecies";
		if(n == 13) return "terdecies";
		if(n == 14) return "quaterdecies";
		if(n == 15) return "quindecies";
		if(n == 16) return "sexdecies";
		if(n == 17) return "septdecies";
		if(n == 18) return "octodecies";
		if(n == 19) return "novodecies";
		if(n == 20) return "vicies";
		if(n == 21) return "unvicies";
		if(n == 22) return "duovicies";
		if(n == 23) return "tervicies";
		if(n == 24) return "quatervicies";
		if(n == 25) return "quinvicies";
		
		return null;
	}
	
	static final String readFirstNumber(String text) {
		
		if(text == null) return null;
		
		Matcher matcher = digits.matcher(text);
		String number = "";
		
		while(matcher.find()) {
			
			number = text.substring(matcher.start(), matcher.end());			
			break;
		}
		
		return number;
	}

	static final String readSecondNumber(String text) {
		
		if(text == null) return null;
		
		Matcher matcher = digits.matcher(text);
		String number = "";
		
		boolean first = false;
		while(matcher.find()) {
			
			number = text.substring(matcher.start(), matcher.end());
			if(first) break;
			first = true;
		}
		
		return number;
	}

	static final String readLastNumber(String text) {
		
		Matcher matcher = digits.matcher(text);
		String number = "";
		
		while( matcher.find() ) {
			number = text.substring(matcher.start(), matcher.end());
		}
		
		return number;
	}
	
	static String normalizeYear(String year) {
		
		if(year.length() == 2) {

			int value = Integer.valueOf(year);

			if(value < 25) {
				
				year = "20" + year;
				
			} else {
				
				year = "19" + year;
			}
		}
		
		return year;
	}
	
	static String normalizeCompleteDate(String completeDate) {
		
		String day = Util.readFirstNumber(completeDate);
		String year = Util.readLastNumber(completeDate);
		
		if(day.length() == 1) day = "0" + day;

		String month = "";

		if(completeDate.toLowerCase().indexOf("gennaio") > -1) month = "01";
		if(completeDate.toLowerCase().indexOf("febbraio") > -1) month = "02";
		if(completeDate.toLowerCase().indexOf("marzo") > -1) month = "03";
		if(completeDate.toLowerCase().indexOf("aprile") > -1) month = "04";
		if(completeDate.toLowerCase().indexOf("maggio") > -1) month = "05";
		if(completeDate.toLowerCase().indexOf("giugno") > -1) month = "06";
		if(completeDate.toLowerCase().indexOf("luglio") > -1) month = "07";
		if(completeDate.toLowerCase().indexOf("agosto") > -1) month = "08";
		if(completeDate.toLowerCase().indexOf("settembre") > -1) month = "09";
		if(completeDate.toLowerCase().indexOf("ottobre") > -1) month = "10";
		if(completeDate.toLowerCase().indexOf("novembre") > -1) month = "11";
		if(completeDate.toLowerCase().indexOf("dicembre") > -1) month = "12";

		if(month.equals("")) return null;
		
		return year + "-" + month + "-" + day;
	}

	static String stripAccents(String s) {
		
		s = Normalizer.normalize(s, Normalizer.Form.NFD);
		s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
		return s;
	}
	
	static final String checkNumberedItems(MarkerDocument markerDocument, String text) {
		
		CheckNumberedItems cn = new CheckNumberedItems();
		cn.setInput(text);
		cn.setMarkerDocument(markerDocument);
		cn.run();
		
		return cn.getOutput();
	}
	
	static final String removeAllAnnotations(String text) {
		
		Cleaner c = new Cleaner();
		
		try {
			
			c.yyreset(new StringReader(text));
			
			c.yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			
			return "";
		}
		
		CleanerBlanks cb = new CleanerBlanks();
		
		try {
			
			cb.yyreset(new StringReader(c.getOutput()));
			
			cb.yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			
			return "";
		}
		
		return cb.getOutput();
	}
	
	static final String getOnlyMeaningfulText(String text) {
		
		CleanMeaningless cm = new CleanMeaningless();
		
		try {
			
			cm.yyreset(new StringReader(text));
			
			cm.yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			
			return "";
		}
		
		return cm.getOutput();
	}
	
	static String getEqualPart(String a, String b) {
		
		for(int i = 0; i < a.length(); i++) {
			
			Character ai = a.charAt(i);
			
			if(i >= b.length()) {
				
				return a.substring(0, i);
			}
			
			Character bi = b.charAt(i);

			if( !ai.equals(bi)) {
				
				return a.substring(0, i);
			}
		}
		
		return a;
	}

	private static Map<String,String> word2accent = null;
	
	private static boolean loadWords() {
		
		word2accent = new HashMap<String,String>();
		
		try {
			
			BufferedReader reader = null;
			InputStream in = null;

			File wordsFile = Paths.get("data/original-pruned.txt").toFile();
			
			if( !wordsFile.exists()) {
				
				in = MarkerDocument.class.getResourceAsStream("/data/original-pruned.txt");
				
				if(in == null) return false;
				
				reader = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
			
			} else {
				
				reader = new BufferedReader(new InputStreamReader(new FileInputStream(wordsFile), StandardCharsets.UTF_8));
			}
			
			String line = null;

		    while( ( line = reader.readLine() ) != null ) {
		    	
				line = line.trim().toLowerCase();
				
				if(line.length() < 1) continue;
				
				String[] items = line.split(" ");
				
				if(items.length < 2) continue;
				
				String word = items[0];
				String lastChar = items[1];
				
				word2accent.put(word + "'", lastChar);
			}

		    reader.close();  
		    if(in != null) in.close();
			
			wordsFile = Paths.get("data/custom-words.txt").toFile();
			
			if( !wordsFile.exists()) {
				
				in = MarkerDocument.class.getResourceAsStream("/data/custom-words.txt");
				
				if(in == null) return false;
				
				reader = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
			
			} else {
				
				reader = new BufferedReader(new InputStreamReader(new FileInputStream(wordsFile), StandardCharsets.UTF_8));
			}
			
			line = null;

		    while( ( line = reader.readLine() ) != null ) {
		    	
				line = line.trim().toLowerCase();
				
				if(line.length() < 1) continue;
				
				String[] items = line.split("\t");
				
				if(items.length < 2) continue;
				
				String word = items[0];
				String lastChar = items[1];
				
				word2accent.put(word + "'", lastChar);
			}
			
		    reader.close();  
		    if(in != null) in.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return true;
	}
	
	static String getAccent(String word) {
		
		if(word == null) return null;
		
		word = word.trim().toLowerCase();
		
		if(word.length() < 1) return null;
		
		if(word2accent == null) {
			
			if( !loadWords()) {
				
				System.err.println("Error loading word2accent.");
				return null;
			}
		}
		
		return word2accent.get(word);
	}

	static String normalizeCharSet(String charSet) {
		
		if(charSet == null) return null;

		//charSet possibili: "UTF-8", "ISO-8859-1", "windows-1252"
		
		charSet = charSet.trim();
			
		if(charSet.equalsIgnoreCase("utf-8")) return "UTF-8";
		
		if(charSet.equalsIgnoreCase("windows-1252")) return "windows-1252";
		
		if(charSet.equalsIgnoreCase("iso-8859-1")) return "iso-8859-1";
		
		return null;
	}


}
