/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.marker;

import java.util.HashMap;
import java.util.Map;

public class Context {
	
	/*
	 * Informazioni di contesto sul documento in input
	 * e sugli step del parsing
	 */
	
	Context() { }
	
	private MarkerDocument markerDocument = null;
	
	MarkerDocument getMarkerDocument() {
		return markerDocument;
	}

	void setMarkerDocument(MarkerDocument markerDocument) {
		
		this.markerDocument = markerDocument;
	}
	
	private String currentPartType = "";
	
	String getCurrentPartType() {
		return currentPartType;
	}

	void setCurrentPartType(String currentPartType) {
		this.currentPartType = currentPartType;
	}

	private String quotesBaseId = "";

	/*
	 * Utilizzato per memorizzare il contenitore in caso di virgolette seguite da testo seguite da altre virgolette
	 * (teoricamente non dovrebbe accadere e di solito significa che c'è un errore, in questi casi il lastQuotesBaseId
	 * è comunque utile per fare la segnalazione correttamente)
	 */
	private String lastQuotesBaseId = ""; 
	
	String getQuotesBaseId() {
		return quotesBaseId;
	}
	
	private String annexBaseId = "";

	String getAnnexBaseId() {
		return annexBaseId;
	}
	
	private boolean firstInQuotesPartition = true;

	private String containerId = "";
	
	String getContainerId() {
		
		return this.containerId;
	}
	
	void setContainerId(String containerId) {
		
		this.containerId = containerId;
	}
		
	void updateContainer(String containerAnnotation) {
		
		int cut = containerAnnotation.indexOf("id=\"");
		
		if(cut > -1) {
			
			containerId = containerAnnotation.substring(cut + "id=\"".length());
			containerId = containerId.substring(0, containerId.indexOf("\""));
		}
	}
	
	void updateQuotesContainer(String containerAnnotation) {
		
		updateContainer(containerAnnotation);
		
		//quotesBaseId = containerId + "_quotes_";
		quotesBaseId = containerId + "_virgolette_";
		lastQuotesBaseId = quotesBaseId;
	}
	
	void updateAnnexContainer(String annexContainerAnnotation) {
		
		updateContainer(annexContainerAnnotation);
		
		annexBaseId = containerId;
	}
	
	void keepLastQuotesBaseId() {
		
		quotesBaseId = lastQuotesBaseId;
	}
	
	private boolean commiNonNumerati = false;
	
	boolean isCommiNonNumerati() {
		return commiNonNumerati;
	}

	void setCommiNonNumerati(boolean commiNonNumerati) {
		this.commiNonNumerati = commiNonNumerati;
	}

	private boolean inQuotes = false;
	private boolean inAnnex = false;
	
	//Indica che le partizioni inferiori all'articolo devono essere cercate in tutto il testo delle virgolette
	//(se ci sono annotazioni articolo o inferiore si seguono le regole standard di ReadPart)
	private boolean plainQuotes = true;
	
	boolean isInQuotes() {
		return inQuotes;
	}

	void setInQuotes(boolean inQuotes) {
		this.inQuotes = inQuotes;
	}

	boolean isInAnnex() {
		return inAnnex;
	}

	void setInAnnex(boolean inAnnex) {
		this.inAnnex = inAnnex;
	}

	boolean isPlainQuotes() {
		return plainQuotes;
	}

	void setPlainQuotes(boolean plainQuotes) {
		this.plainQuotes = plainQuotes;
	}
	
	/*
	 * Questa mappa è usata per salvare le informazioni di contesto relative ad un singolo articolo,
	 * in modo da poter in seguito effettuare dei guess sulla strategia di analisi dell'articolo,
	 * in particolare se ha commi numerati oppure non ce li ha, e di conseguenza come sistemare
	 * eventuali rubriche.
	 */
	private Map<String,ArticoloContext> id2articoloContext = new HashMap<String,ArticoloContext>();
	
	void addArticoloContext(String id, ArticoloContext articoloContext) {
		
		id2articoloContext.put(id, articoloContext);
	}
	
	ArticoloContext getArticoloContext(String id) {
		
		return id2articoloContext.get(id);
	}
	
	private int oneMeaningfulNotNumberedParagraph = -1;
	
	private int oneCommaWithoutRubrica = -1;
	
	boolean isThereOneMeaningfulNotNumberedParagraph() {
		
		if(oneMeaningfulNotNumberedParagraph == 0) return false;
		if(oneMeaningfulNotNumberedParagraph == 1) return true;
		
		for(ArticoloContext articoloContext : id2articoloContext.values()) {
			
			if( !articoloContext.isCommiNumerati() && !articoloContext.isFirstParagraphInBrackets() && articoloContext.isOneMeaningfulParagraph()) {
				
				oneMeaningfulNotNumberedParagraph = 1;
				return true;
			}
		}
		
		oneMeaningfulNotNumberedParagraph = 0;
		return false;
	}
	
	boolean isThereOneCommaWithoutRubrica() {
		
		if(oneCommaWithoutRubrica == 0) return false;
		if(oneCommaWithoutRubrica == 1) return true;
		
		for(ArticoloContext articoloContext : id2articoloContext.values()) {
			
			if(articoloContext.isCommiNumerati() && !articoloContext.isFirstParagraphInBrackets() && articoloContext.isNothingBeforeComma()) {
				
				oneCommaWithoutRubrica = 1;
				return true;
			}
		}
		
		oneCommaWithoutRubrica = 0;
		return false;
	}
	
	
	/*
	 * Controlli di sequenza delle partizioni 
	 */
	
	//Per gli allegati non si fa il controllo sequenza, c'è solo il controllo di univocità dell'ID alla fine con eventuale segnalazione
	//private Set<String> annexCodes = new HashSet<String>();
	//private Set<String> tableCodes = new HashSet<String>();
	
	private int book = 0;
	
	private int part = 0;
	
	private int title = 0;
	private int titleLatin = 0;
	
	private int chapter = 0;
	private int chapterLatin = 0;
	
	private int section = 0;
	private int sectionLatin = 0;
	
	private int article = 0;
	private int articleLatin = 0;
	private int articleSubNumber = 0;
	
	private int comma = 0;
	private int commaLatin = 0;
	private int commaSubNumber = 0;
	
	private int paragraph = 0;
	
	private int letter = 0;
	private int letterLatin = 0;
	
	private int number = 0;
	private int numberLatin = 0;
	
	//latin list: i) ii) iii)
	private int latin = 0; 
	
	private int subNumberItem = 0; 	// 1.1 1.2 1.3 - Salva solo il secondo numero - TODO può dar luogo ad errori ed ambiguità in rari casi
	
	private String bookHeader = "";
	private String partHeader = "";
	private String titleHeader = "";
	private String chapterHeader = "";
	private String sectionHeader = "";
	private String articleHeader = "";
	private String commaHeader = "";
	private String letterHeader = "";
	private String numberHeader = "";
	private String latinHeader = "";
	private String subNumberHeader = "";
	

	
	String getCurrentPartId(int numero, String letter, int latin, int subNumber) {
		
		if(currentPartType.equals("")) return "";
		
		String id = "";
		
		if( !containerId.equals("")) {
			
			if(currentPartType.equalsIgnoreCase("articolo")) {

				// Nel documento principale l'articolo è identificato univocamente senza bisogno dei container superiori.
				
			} else {
				
				id = containerId + "_";
			}
		}

		if(currentPartType.equalsIgnoreCase("libro")) id += "libro";
		if(currentPartType.equalsIgnoreCase("parte")) id += "parte";
		if(currentPartType.equalsIgnoreCase("titolo")) id += "titolo";
		if(currentPartType.equalsIgnoreCase("capo")) id += "capo";
		if(currentPartType.equalsIgnoreCase("sezione")) id += "sezione";
		if(currentPartType.equalsIgnoreCase("articolo")) id += "articolo";
		if(currentPartType.equalsIgnoreCase("comma")) id += "comma";
		if(currentPartType.equalsIgnoreCase("paragrafo")) id += "paragrafo";
		if(currentPartType.equalsIgnoreCase("lettera")) id += "lettera";
		if(currentPartType.equalsIgnoreCase("numero")) id += "numero";
		if(currentPartType.equalsIgnoreCase("numeroLatino")) id += "numeroLatino";
		if(currentPartType.equalsIgnoreCase("sottoNumero")) id += "sottoNumero";

		if(currentPartType.equalsIgnoreCase("allegato") || currentPartType.equalsIgnoreCase("articolo")) {
			
			if(numero == 0) {
				
				//do nothing?
				
			} else if(numero == 9999) {
				
				//articolo unico
				id += "-unico";
				
			} else {
				
				id += "-" + numero;
			}
		}

		if(currentPartType.equalsIgnoreCase("libro") || currentPartType.equalsIgnoreCase("parte") || currentPartType.equalsIgnoreCase("titolo") ||
				currentPartType.equalsIgnoreCase("capo") || currentPartType.equalsIgnoreCase("sezione")) {
			
			id += "-" + numero;
		}
		
		if(currentPartType.equalsIgnoreCase("comma") || currentPartType.equalsIgnoreCase("numero")) {
			
			id += "-" + numero;
		}
		
		if(currentPartType.equalsIgnoreCase("paragrafo")) {
			
			paragraph++;
			
			//id += "-" + Util.getRomanNumeralFromInteger(paragraph).toLowerCase();
			id += "-" + paragraph;
		}

		if(currentPartType.equalsIgnoreCase("lettera")) {
			
			id += "-" + letter.toLowerCase();
		}
		
		if(currentPartType.equalsIgnoreCase("numeroLatino")) {
			
			id += "-" + letter.toLowerCase();
		}
		
		if(latin > 0) {
			
			id += "-" + Util.getRomanSuffixFromInteger(latin);
		}

		if(subNumber > 0) {
			
			id += "-" + subNumber;
		}

		//All'interno delle virgolette aggiungi il baseId come prefisso nel caso non sia già presente
		//if(inQuotes && id.indexOf("quote") < 0) {
		if(inQuotes && id.indexOf("virgolette") < 0) {
			
			id = quotesBaseId + id;
		}
		
		//Negli allegati aggiungi annexBaseId come prefisso nel caso non sia già presente
		if(inAnnex) {
			
			//if( !id.startsWith("annex")) {
			if( !id.startsWith("allegato")) {
				
				id = annexBaseId + "_" + id;
			}
		}
		
		return id;
	}
	
	void resetAll() {
		
		resetBook();
		resetPart();
		resetTitle();
		resetChapter();
		resetSection();
		resetArticle();
		resetComma();
		resetLetter();
		resetNumber();
		resetSubNumberItem();
		resetLatin();
		
		containerId = "";
		
		commiNonNumerati = false;
		
		id2articoloContext = new HashMap<String,ArticoloContext>();
		oneMeaningfulNotNumberedParagraph = -1;
		oneCommaWithoutRubrica = -1;
	}
	
	void resetCurrent() {
		
		//L'articolo ha numerazione unica, non resettare per il controllo sequenza
		
		if(currentPartType.equals("libro")) resetBook();
		if(currentPartType.equals("parte")) resetPart();
		if(currentPartType.equals("titolo")) resetTitle();
		if(currentPartType.equals("capo")) resetChapter();
		if(currentPartType.equals("sezione")) resetSection();
		if(currentPartType.equals("comma")) resetComma();
		if(currentPartType.equals("paragrafo")) resetParagraph();
		if(currentPartType.equals("lettera")) resetLetter();
		if(currentPartType.equals("numero")) resetNumber();
		if(currentPartType.equals("sottoNumero")) resetSubNumberItem();
		if(currentPartType.equals("numeroLatino")) resetLatin();
	}
	
	void resetQuotes() {
		
		quotesBaseId = "";
		firstInQuotesPartition = true;
		plainQuotes = true;
	}
	
	void resetBook() {
		
		book = 0;
		bookHeader = "";
	}

	void resetPart() {
		
		part = 0;
		partHeader = "";
	}
	
	void resetTitle() {

		title = 0;
		titleLatin = 0;
		titleHeader = "";
	}
	
	void resetChapter() {
		
		chapter = 0;
		chapterLatin = 0;
		chapterHeader = "";
	}

	void resetSection() {
		
		section = 0;
		sectionLatin = 0;
		sectionHeader = "";
	}

	void resetArticle() {
		
		article = 0;
		articleLatin = 0;
		articleSubNumber = 0;
		articleHeader = "";
	}

	void resetComma() {
		
		comma = 0;
		commaLatin = 0;
		commaHeader = "";
	}

	void resetParagraph() {
		
		paragraph = 0;
	}

	void resetLetter() {
		
		letter = 0;
		letterLatin = 0;
		letterHeader = "";
	}

	void resetNumber() {
		
		number = 0;
		numberLatin = 0;
		numberHeader = "";
	}

	void resetLatin() {
		
		latin = 0;
		latinHeader = "";
	}

	void resetSubNumberItem() {
		
		subNumberItem = 0;
		subNumberHeader = "";
	}
	
	//latinCheckOnly true usato per disambiguare tra lista letterale e lista latin senza aggiornare i valori di contesto letter o latin
	boolean checkCurrentPart(int numero, int latin, int subNumber, boolean latinCheckOnly, String header) {
		
		//System.out.println("CHECK " + currentPartType + " numero: " + numero + " latin: " + latin);
		//System.out.println("CURRENT letter: " + letter + " letterLatin: " + letterLatin);
		//System.out.println("CURRENT inQuotes: " + inQuotes + " firstInQuotesPartition: " + firstInQuotesPartition);
		
		if(currentPartType.equals("libro")) return checkBook(header, numero);
		if(currentPartType.equals("parte")) return checkPart(header, numero);
		if(currentPartType.equals("titolo")) return checkTitle(header, numero);
		if(currentPartType.equals("capo")) return checkChapter(header, numero, latin);
		if(currentPartType.equals("sezione")) return checkSection(header, numero, latin);
		if(currentPartType.equals("articolo")) return checkArticle(header, numero, latin, subNumber);
		if(currentPartType.equals("comma")) return checkComma(header, numero, latin, subNumber);
		if(currentPartType.equals("lettera")) return checkLetter(header, numero, latin);
		if(currentPartType.equals("numero")) return checkNumber(header, numero, latin);
		if(currentPartType.equals("numeroLatino")) return checkLatin(header, numero, latinCheckOnly);
		if(currentPartType.equals("sottoNumero")) return checkSubNumberItem(header, subNumber); //controlla solo il secondo numero (es.: 1.3)
		
		return false;
	}

	String checkAndGetCurrentAnnexId(String code) {
		
		code = code.toLowerCase().trim();
		
		String id = "";
		
		//if(currentPartType.equalsIgnoreCase("allegato")) id = "annex";
		//if(currentPartType.equalsIgnoreCase("tabella")) id = "table";
		if(currentPartType.equalsIgnoreCase("allegato")) id = "allegato";
		if(currentPartType.equalsIgnoreCase("tabella")) id = "tabella";

		if(code.length() > 0) id += "-" + code;

		return id;
	}
	
	private boolean checkBook(String header, int numero) {
		
		if(numero < 1) return false;
		
		if(numero == (book+1)) {
			
			book = numero;
			bookHeader = header;
			
			return true;
		}
		
		if(getMarkerDocument().isDebug()) System.out.println("idref:" + containerId + " last book: " + book + " vs read: " + numero);
		getMarkerDocument().addWarning("idref:" + containerId + " contenitore di tipo Libro trovato nel testo: \"" + header + "\" VS ultimo libro in sequenza: \"" + bookHeader + "\"");
		
		return false;
	}
		
	private boolean checkPart(String header, int numero) {
		
		if(numero < 1) return false;
		
		if(numero == (part+1)) {
			
			part = numero;
			partHeader = header;
			
			return true;
		}
		
		if(getMarkerDocument().isDebug()) System.out.println("idref:" + containerId + " last part: " + part + " vs read: " + numero);
		getMarkerDocument().addWarning("idref:" + containerId + " contenitore di tipo Parte trovato nel testo: \"" + header + "\" VS ultima parte in sequenza: \"" + partHeader + "\"");
		
		return false;
	}

	private boolean checkTitle(String header, int numero) {
		
		if(numero < 1) return false;

		if(inQuotes && firstInQuotesPartition) {
			
			title = numero;
			titleLatin = latin;
			firstInQuotesPartition = false;
			
			titleHeader = header;
			
			return true;
		}
		
		if(numero == (title+1)) {
			
			title = numero;
			titleHeader = header;
			
			return true;
		}
		
		if(getMarkerDocument().isDebug()) System.out.println("idref:" + containerId + " last title: " + title + "-" + titleLatin + " vs read: " + numero + "-" + latin);
		getMarkerDocument().addWarning("idref:" + containerId + " contenitore di tipo Titolo trovato nel testo: \"" + header + "\" VS ultimo titolo in sequenza: \"" + titleHeader + "\"");
		
		return false;
	}
	
	private boolean checkChapter(String header, int numero, int latin) {
		
		if(numero < 1 || latin < 0) return false;
		
		if(inQuotes && firstInQuotesPartition) {
			
			chapter = numero;
			chapterLatin = latin;
			firstInQuotesPartition = false;
			
			chapterHeader = header;
			
			return true;
		}
	
		if(numero == chapter) {
			
			//Se il numero non varia controlla il valore latin (bis, ter, etc.)
			if((chapterLatin == 0 && latin == 2) || latin == (chapterLatin+1)) {
			
				chapterLatin = latin;
				
				chapterHeader = header;
				
				return true;
			}
		}
		
		if(numero == (chapter+1) && latin == 0) {
			
			chapter = numero;
			chapterLatin = 0;
			
			chapterHeader = header;
			
			return true;
		}
		
		if(getMarkerDocument().isDebug()) System.out.println("idref:" + containerId + " last chapter: " + chapter + "-" + chapterLatin + " vs read: " + numero + "-" + latin);
		getMarkerDocument().addWarning("idref:" + containerId + " contenitore di tipo Capo trovato nel testo: \"" + header + "\" VS ultimo capo in sequenza: \"" + chapterHeader + "\"");
		
		return false;
	}

	private boolean checkSection(String header, int numero, int latin) {
		
		if(numero < 1) return false;
		
		if(inQuotes && firstInQuotesPartition) {
			
			section = numero;
			sectionLatin = latin;
			firstInQuotesPartition = false;
			
			sectionHeader = header;
			
			return true;
		}

		if(numero == section) {
			
			//Se il numero non varia controlla il valore latin (bis, ter, etc.)
			if((sectionLatin == 0 && latin == 2) || latin == (sectionLatin+1)) {
			
				sectionLatin = latin;
				
				sectionHeader = header;
				
				return true;
			}
		}

		if(numero == (section+1) && latin == 0) {
			
			section = numero;
			sectionLatin = 0;
			
			sectionHeader = header;
			
			return true;
		}
		
		if(getMarkerDocument().isDebug()) System.out.println("idref:" + containerId + " last section: " + section + "-" + sectionLatin + " vs read: " + numero + "-" + latin);
		getMarkerDocument().addWarning("idref:" + containerId + " contenitore di tipo Sezione trovato nel testo: \"" + header + "\" VS ultima sezione in sequenza: \"" + sectionHeader + "\"");
		
		return false;
	}
	
	private boolean checkArticle(String header, int numero, int latin, int subNumber) {
		
		if(numero == 9999) {
			
			//codice per "Articolo Unico"
			
			if(article == 0) {
				
				article = numero;
				articleHeader = header;
				
				return true;
			}
		}
		
		if(numero < 1 || latin < 0) return false;
		
		//A differenza delle altre partizioni, l'articolo può iniziare sempre da qualsiasi numero
		//all'interno delle virgolette, nella sua prima dichiarazione
		if(inQuotes && article == 0) {
			
			article = numero;
			articleLatin = latin;
			articleSubNumber = subNumber;
			firstInQuotesPartition = false;
			articleHeader = header;
			
			return true;
		}
		
		if(numero == article) {
			
			//Se il numero non varia controlla il valore latin (bis, ter, etc.)
			if((articleLatin == 0 && latin == 2) || latin == (articleLatin+1)) {
			
				articleLatin = latin;
				articleHeader = header;
				
				return true;
			}
			
			if(articleLatin == latin) {
				
				if((articleSubNumber == 0 && subNumber == 2) || subNumber == (articleSubNumber+1)) {
					
					articleSubNumber = subNumber;
					articleHeader = header;
					
					return true;
				}
			}
		}
		
		if(numero == (article+1) && latin == 0 && subNumber == 0) {
			
			article = numero;
			articleLatin = 0;
			articleSubNumber = 0;
			articleHeader = header;
			
			return true;
		}
		
		if(getMarkerDocument().isDebug()) System.out.println("Context Warning in " + containerId + ": Last article-latin-sub: " + article + "-" + articleLatin + "-" + articleSubNumber + " vs read: " + numero + "-" + latin + "-" + subNumber);
		getMarkerDocument().addWarning("idref:" + containerId + " Articolo trovato nel testo: \"" + header + "\" VS ultimo articolo in sequenza: \"" + articleHeader + "\"");
		
		return false;
	}

	private boolean checkComma(String header, int numero, int latin, int subNumber) {
		
		if(numero < 1 || latin < 0) return false;
		
		if(inQuotes && firstInQuotesPartition) {
			
			comma = numero;
			commaLatin = latin;
			commaSubNumber = subNumber;
			firstInQuotesPartition = false;
			commaHeader = header;
			
			return true;
		}

		if(numero == comma) {
			
			//Se il numero non varia controlla il valore latin (bis, ter, etc.)
			if((commaLatin == 0 && latin == 2) || latin == (commaLatin+1)) {
			
				commaLatin = latin;
				commaHeader = header;
			
				return true;
			}
			
			if(commaLatin == latin) {
				
				if((commaSubNumber == 0 && subNumber == 2) || subNumber == (commaSubNumber+1)) {
					
					commaSubNumber = subNumber;
					commaHeader = header;
					
					return true;
				}
			}
		}
		
		if(numero == (comma+1) && latin == 0) {
			
			comma = numero;
			commaLatin = 0;
			commaSubNumber = 0;
			commaHeader = header;
			
			return true;
		}
		
		if(getMarkerDocument().isDebug()) System.out.println("Context Warning in " + containerId + ": Last comma-latin: " + comma + "-" + commaLatin + "-" + commaSubNumber + " vs read: " + numero + "-" + latin + "-" + subNumber);
		//Non stampare segnalazione se header è vuoto: probabile errate rilevazione all'interno di rubriche non correttamente inserite nei paragrafi
		//UPDATE: no, con il nuovo ReadPart e Title non si lancia il rilevamento liste nelle rubriche.
		/*if( !commaHeader.equals("")) */getMarkerDocument().addWarning("idref:" + containerId + " Comma trovato nel testo: \"" + header + "\" VS ultimo comma in sequenza: \"" + commaHeader + "\"");
				
		return false;
	}
	
	private boolean checkLetter(String header, int numero, int latin) {
		
		//Non fare controllo sequenza per le lettere inglesi e le lettere doppie aa) bb) ab) -- OLD
		if(numero == 99) return true;
		
		if(inQuotes && firstInQuotesPartition) {
			
			letter = numero;
			letterLatin = latin;
			firstInQuotesPartition = false;
			letterHeader = header;
			
			return true;
		}

		if(numero < 1 || latin < 0) return false;
		
		if(numero == letter) {

			//Se il numero non varia controlla il valore latin (bis, ter, etc.)
			if((letterLatin == 0 && latin == 2) || latin == (letterLatin+1)) {

				letterLatin = latin;
				letterHeader = header;
				
				return true;
			}
		}
		
		if(numero == (letter+1) && latin == 0) {
			
			letter = numero;
			letterLatin = 0;
			letterHeader = header;
			
			return true;
		}

		//dopo i si accetta sia j che l
		//if(numero == 9 && letter == 12) {
		if(letter == 9 && numero == 12) {
			
			letter = numero;
			letterLatin = 0;
			letterHeader = header;
			
			return true;
		}
		
		//dopo v si accetta sia w che z
		//if(numero == 23 && letter == 26) {
		//v w x y z
		if(letter == 22 && numero == 26) {
			
			letter = numero;
			letterLatin = 0;
			letterHeader = header;
			
			return true;
		}

		//alcune liste dopo z hanno aa), bb), altre hanno ab), ac), ad)
		//Dopo z si accetta sia a che b
		//if(numero == 26 && (letter == 1 || letter == 2)) {
		if(letter == 26 && (numero == 1 || numero == 2)) {
			
			letter = numero;
			letterLatin = 0;
			letterHeader = header;
			
			return true;
		}

		//Se siamo in un documento europeo, non considerare nei check le possibili liste latine i) ii) iii) iv)...
		//escludi i, v ed x:
		if(getMarkerDocument().getInputType().equals(InputType.PLAIN_EUR_LEX) && latin == 0 && (numero == 9 || numero == 22  || numero == 24) ) {
			
			return false;
		}
		
		if(getMarkerDocument().isDebug()) System.out.println("Context Warning in " + containerId + ": Last letter-latin: " + letter + "-" + letterLatin + " vs read: " + numero + "-" + latin);
		//Non stampare segnalazione se header è vuoto: probabile errate rilevazione all'interno di rubriche non correttamente inserite nei paragrafi
		//UPDATE: no, con il nuovo ReadPart e Title non si lancia il rilevamento liste nelle rubriche.
		/*if( !letterHeader.equals("")) */getMarkerDocument().addWarning("idref:" + containerId + " Elemento di lista letterale trovato nel testo: \"" + header + "\" VS ultima lettera in sequenza: \"" + letterHeader + "\"");
		
		return false;
	}


	private boolean checkNumber(String header, int numero, int latin) {
		
		if(numero < 1 || latin < 0) return false;
		
		if(inQuotes && firstInQuotesPartition) {
			
			number = numero;
			numberLatin = latin;
			firstInQuotesPartition = false;
			numberHeader = header;
			
			return true;
		}

		if(numero == number) {
			
			//Se il numero non varia controlla il valore latin (bis, ter, etc.)
			if((numberLatin == 0 && latin == 2) || latin == (numberLatin+1)) {
			
				numberLatin = latin;
				numberHeader = header;
				
				return true;
			}
		}
		
		if(numero == (number+1) && latin == 0) {
			
			number = numero;
			numberLatin = 0;
			numberHeader = header;
			
			return true;
		}
		
		if(getMarkerDocument().isDebug()) System.out.println("Context Warning in " + containerId + ": Last number-latin: " + number + "-" + numberLatin + " vs read: " + numero + "-" + latin);
		//Non stampare segnalazione se header è vuoto: probabile errate rilevazione all'interno di rubriche non correttamente inserite nei paragrafi
		//UPDATE: no, con il nuovo ReadPart e Title non si lancia il rilevamento liste nelle rubriche.
		/*if( !numberHeader.equals("")) */getMarkerDocument().addWarning("idref:" + containerId + " Elemento di lista numerica trovato nel testo: \"" + header + "\" VS ultimo numero in sequenza: \"" + numberHeader + "\"");
		
		return false;
	}
	

	private boolean checkLatin(String header, int numero, boolean checkOnly) {
		
		if(numero < 1) return false;
		
		if(numero == (latin+1)) {
			
			if( !checkOnly) {
			
				latin = numero;
				latinHeader = header;
			}
			
			return true;
		}
		
		if( !checkOnly) {

			if(getMarkerDocument().isDebug()) System.out.println("Context Warning in " + containerId + ": Last latin item: " + latin + " vs read: " + numero);
			getMarkerDocument().addWarning("idref:" + containerId + " Elemento di lista latina trovato nel testo: \"" + header + "\" VS ultimo latino in sequenza: \"" + latinHeader + "\"");
		}
		
		return false;
	}

	private boolean checkSubNumberItem(String header, int numero) {
		
		if(numero < 1) return false;

		if(numero == (subNumberItem+1)) {
			
			subNumberItem = numero;
			subNumberHeader = header;
			
			return true;
		}
		
		if(getMarkerDocument().isDebug()) System.out.println("Context Warning in " + containerId + ": Last subNumberItem item: " + subNumberItem + " vs read: " + numero );
		getMarkerDocument().addWarning("idref:" + containerId + " Elemento di lista sotto-numerica trovato nel testo: \"" + header + "\" VS ultimo sotto-numero in sequenza: \"" + subNumberHeader + "\"");
		
		return false;
	}

}
