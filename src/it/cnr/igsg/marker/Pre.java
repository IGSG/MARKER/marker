// DO NOT EDIT
// Generated by JFlex 1.9.1 http://jflex.de/
// source: jflex/Pre.jflex

/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.marker;


@SuppressWarnings("fallthrough")
public class Pre extends JFlexAnnotationService {

  /** This character denotes the end of file. */
  public static final int YYEOF = -1;

  /** Initial size of the lookahead buffer. */
  private static final int ZZ_BUFFERSIZE = 16384;

  // Lexical states.
  public static final int YYINITIAL = 0;
  public static final int inMrkPre = 2;

  /**
   * ZZ_LEXSTATE[l] is the state in the DFA for the lexical state l
   * ZZ_LEXSTATE[l+1] is the state in the DFA for the lexical state l
   *                  at the beginning of a line
   * l is of the form l = 2*k, k a non negative integer
   */
  private static final int ZZ_LEXSTATE[] = {
     0,  0,  1, 1
  };

  /**
   * Top-level table for translating characters to character classes
   */
  private static final int [] ZZ_CMAP_TOP = zzUnpackcmap_top();

  private static final String ZZ_CMAP_TOP_PACKED_0 =
    "\1\0\1\u0100\36\u0200\1\u0300\1\u0400\1\u0500\u10dd\u0200";

  private static int [] zzUnpackcmap_top() {
    int [] result = new int[4352];
    int offset = 0;
    offset = zzUnpackcmap_top(ZZ_CMAP_TOP_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackcmap_top(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }


  /**
   * Second-level tables for translating characters to character classes
   */
  private static final int [] ZZ_CMAP_BLOCKS = zzUnpackcmap_blocks();

  private static final String ZZ_CMAP_BLOCKS_PACKED_0 =
    "\11\0\1\1\1\2\1\0\1\1\1\3\22\0\1\4"+
    "\5\0\1\5\1\0\1\6\1\7\5\0\1\10\12\0"+
    "\1\11\1\12\1\13\1\0\1\14\2\0\1\15\1\16"+
    "\1\17\1\20\1\21\1\0\1\22\1\0\1\23\1\0"+
    "\1\24\1\25\1\26\1\27\1\30\1\31\1\0\1\32"+
    "\1\33\1\34\1\35\1\36\3\0\1\37\1\40\1\0"+
    "\1\41\3\0\1\15\1\16\1\17\1\20\1\21\1\0"+
    "\1\22\1\0\1\23\1\0\1\24\1\25\1\26\1\27"+
    "\1\30\1\31\1\0\1\32\1\33\1\34\1\35\1\36"+
    "\3\0\1\37\45\0\1\42\217\0\2\23\115\0\1\33"+
    "\u0182\0\1\4\u0127\0\1\24\347\0\1\43\355\0";

  private static int [] zzUnpackcmap_blocks() {
    int [] result = new int[1536];
    int offset = 0;
    offset = zzUnpackcmap_blocks(ZZ_CMAP_BLOCKS_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackcmap_blocks(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }

  /**
   * Translates DFA states to action switch labels.
   */
  private static final int [] ZZ_ACTION = zzUnpackAction();

  private static final String ZZ_ACTION_PACKED_0 =
    "\4\0\2\1\1\2\1\1\1\3\1\4\1\5\1\6"+
    "\4\1\1\7\1\10\6\11\4\0\1\12\113\0\1\13"+
    "\22\0\1\13\2\0\1\13\7\0\1\13\14\0\1\14"+
    "\3\0\1\15\4\0\1\16\3\0";

  private static int [] zzUnpackAction() {
    int [] result = new int[160];
    int offset = 0;
    offset = zzUnpackAction(ZZ_ACTION_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackAction(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }


  /**
   * Translates a state to a row index in the transition table
   */
  private static final int [] ZZ_ROWMAP = zzUnpackRowMap();

  private static final String ZZ_ROWMAP_PACKED_0 =
    "\0\0\0\44\0\110\0\154\0\220\0\264\0\330\0\374"+
    "\0\264\0\u0120\0\220\0\220\0\u0144\0\u0168\0\u018c\0\u01b0"+
    "\0\220\0\220\0\220\0\u01d4\0\u01f8\0\u021c\0\u0240\0\u0264"+
    "\0\110\0\u0288\0\u02ac\0\264\0\330\0\u02d0\0\u0120\0\u01b0"+
    "\0\u02f4\0\u0318\0\u033c\0\u0360\0\u0384\0\u03a8\0\u03cc\0\u01d4"+
    "\0\u01f8\0\u021c\0\u0240\0\u0264\0\u03f0\0\u0414\0\u0438\0\u045c"+
    "\0\220\0\u0480\0\u04a4\0\u04c8\0\u04ec\0\u0510\0\u0534\0\u0558"+
    "\0\u057c\0\u05a0\0\u05c4\0\u05e8\0\u060c\0\u0630\0\u0654\0\u0678"+
    "\0\u069c\0\u06c0\0\u06e4\0\u0708\0\u072c\0\u0750\0\u0774\0\u0798"+
    "\0\u07bc\0\u07e0\0\u0804\0\u0828\0\u084c\0\u0870\0\u0894\0\u08b8"+
    "\0\u08dc\0\u0900\0\u0924\0\u0948\0\u096c\0\u0990\0\u09b4\0\u09d8"+
    "\0\u09fc\0\u0a20\0\u0a44\0\u0a68\0\u0a8c\0\u0ab0\0\u0ad4\0\u0af8"+
    "\0\u0b1c\0\u0b40\0\u0b64\0\u0b88\0\u0bac\0\u0bd0\0\u0bf4\0\u0c18"+
    "\0\u0c3c\0\u0c60\0\u0c84\0\u0ca8\0\u0ccc\0\u0c3c\0\u0cf0\0\u0d14"+
    "\0\u0d38\0\u0d5c\0\u0d80\0\u0da4\0\u0dc8\0\u0dec\0\u0e10\0\u0e34"+
    "\0\u0e58\0\u0e7c\0\u0ea0\0\u0ec4\0\u0ee8\0\u0f0c\0\u0f30\0\u0f54"+
    "\0\u0f78\0\u0f9c\0\u0fc0\0\u0fe4\0\u1008\0\u102c\0\220\0\u1050"+
    "\0\u1074\0\u1098\0\u10bc\0\u10e0\0\u1104\0\u1128\0\u114c\0\u1170"+
    "\0\u1194\0\u11b8\0\u11dc\0\220\0\u1200\0\u1224\0\u1248\0\220"+
    "\0\u126c\0\u1290\0\u12b4\0\u12d8\0\220\0\u12fc\0\u1320\0\u1344";

  private static int [] zzUnpackRowMap() {
    int [] result = new int[160];
    int offset = 0;
    offset = zzUnpackRowMap(ZZ_ROWMAP_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackRowMap(String packed, int offset, int [] result) {
    int i = 0;  /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length() - 1;
    while (i < l) {
      int high = packed.charAt(i++) << 16;
      result[j++] = high | packed.charAt(i++);
    }
    return j;
  }

  /**
   * The transition table of the DFA
   */
  private static final int [] ZZ_TRANS = zzUnpacktrans();

  private static final String ZZ_TRANS_PACKED_0 =
    "\1\5\1\6\1\7\1\10\1\11\1\12\5\5\1\13"+
    "\1\14\3\5\1\15\2\5\1\16\6\5\1\17\5\5"+
    "\1\20\1\5\1\21\1\22\1\23\1\24\1\25\1\26"+
    "\1\24\1\27\32\23\1\30\3\23\1\0\1\31\2\0"+
    "\1\31\1\32\40\0\1\33\106\0\1\34\1\35\1\36"+
    "\1\34\1\37\76\0\1\40\5\0\1\7\35\0\1\40"+
    "\32\0\1\41\35\0\1\42\1\0\1\43\47\0\1\44"+
    "\35\0\1\45\1\0\1\46\46\0\1\47\16\0\1\50"+
    "\1\51\1\52\1\50\1\53\76\0\1\54\5\0\1\51"+
    "\35\0\1\54\32\0\1\55\24\0\1\56\15\0\1\57"+
    "\44\0\1\60\17\0\1\61\42\0\1\35\35\0\1\40"+
    "\21\0\1\62\44\0\1\63\56\0\1\64\31\0\1\65"+
    "\45\0\1\66\54\0\1\67\42\0\1\70\27\0\1\71"+
    "\53\0\1\72\47\0\1\73\27\0\1\74\60\0\1\75"+
    "\33\0\1\76\41\0\1\77\45\0\1\100\50\0\1\101"+
    "\43\0\1\102\37\0\1\103\52\0\1\104\42\0\1\105"+
    "\35\0\1\106\52\0\1\107\41\0\1\110\45\0\1\111"+
    "\44\0\1\112\41\0\1\113\36\0\1\114\43\0\1\115"+
    "\27\0\1\116\63\0\1\117\36\0\1\120\30\0\1\121"+
    "\63\0\1\122\24\0\1\34\54\0\1\123\54\0\1\124"+
    "\32\0\1\125\35\0\1\126\63\0\1\127\41\0\1\130"+
    "\22\0\1\50\42\0\1\131\65\0\1\132\22\0\1\31"+
    "\61\0\1\133\36\0\1\134\57\0\1\135\32\0\1\136"+
    "\54\0\1\111\30\0\1\137\52\0\1\140\34\0\1\141"+
    "\46\0\1\142\52\0\1\143\44\0\1\144\25\0\1\145"+
    "\45\0\1\146\44\0\1\147\42\0\1\150\41\0\1\151"+
    "\37\0\1\151\56\0\1\151\42\0\1\152\45\0\1\153"+
    "\35\0\1\154\51\0\1\155\13\0\1\156\2\0\1\156"+
    "\1\157\1\160\7\0\1\161\1\162\1\0\1\163\7\0"+
    "\1\164\3\0\1\165\42\0\1\144\10\0\1\153\2\0"+
    "\1\153\1\166\23\0\1\167\43\0\1\170\13\0\1\155"+
    "\2\0\1\155\1\171\23\0\1\172\41\0\1\173\32\0"+
    "\1\161\1\162\1\0\1\163\7\0\1\164\3\0\1\165"+
    "\25\0\1\165\45\0\1\174\57\0\1\175\27\0\1\176"+
    "\43\0\1\177\51\0\1\200\46\0\1\201\12\0\1\170"+
    "\2\0\1\170\1\202\23\0\1\203\41\0\1\204\46\0"+
    "\1\205\27\0\1\206\34\0\1\207\7\0\1\210\1\0"+
    "\1\177\54\0\1\211\44\0\1\212\17\0\1\207\52\0"+
    "\1\213\46\0\1\214\51\0\1\215\46\0\1\216\27\0"+
    "\1\217\46\0\1\220\55\0\1\221\25\0\1\177\43\0"+
    "\1\222\45\0\1\177\57\0\1\223\51\0\1\224\20\0"+
    "\1\225\46\0\1\226\55\0\1\227\51\0\1\230\33\0"+
    "\1\231\46\0\1\232\40\0\1\233\45\0\1\234\51\0"+
    "\1\235\33\0\1\236\24\0\1\156\61\0\1\237\25\0"+
    "\1\153\62\0\1\240\24\0\1\155\57\0\1\177\27\0"+
    "\1\170\31\0";

  private static int [] zzUnpacktrans() {
    int [] result = new int[4968];
    int offset = 0;
    offset = zzUnpacktrans(ZZ_TRANS_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpacktrans(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      value--;
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }


  /** Error code for "Unknown internal scanner error". */
  private static final int ZZ_UNKNOWN_ERROR = 0;
  /** Error code for "could not match input". */
  private static final int ZZ_NO_MATCH = 1;
  /** Error code for "pushback value was too large". */
  private static final int ZZ_PUSHBACK_2BIG = 2;

  /**
   * Error messages for {@link #ZZ_UNKNOWN_ERROR}, {@link #ZZ_NO_MATCH}, and
   * {@link #ZZ_PUSHBACK_2BIG} respectively.
   */
  private static final String ZZ_ERROR_MSG[] = {
    "Unknown internal scanner error",
    "Error: could not match input",
    "Error: pushback value was too large"
  };

  /**
   * ZZ_ATTRIBUTE[aState] contains the attributes of state {@code aState}
   */
  private static final int [] ZZ_ATTRIBUTE = zzUnpackAttribute();

  private static final String ZZ_ATTRIBUTE_PACKED_0 =
    "\4\0\1\11\5\1\2\11\4\1\3\11\6\1\1\0"+
    "\1\1\1\0\1\1\23\0\1\11\67\0\1\1\22\0"+
    "\1\1\2\0\1\1\7\0\1\11\14\0\1\11\3\0"+
    "\1\11\4\0\1\11\3\0";

  private static int [] zzUnpackAttribute() {
    int [] result = new int[160];
    int offset = 0;
    offset = zzUnpackAttribute(ZZ_ATTRIBUTE_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackAttribute(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }

  /** Input device. */
  private java.io.Reader zzReader;

  /** Current state of the DFA. */
  private int zzState;

  /** Current lexical state. */
  private int zzLexicalState = YYINITIAL;

  /**
   * This buffer contains the current text to be matched and is the source of the {@link #yytext()}
   * string.
   */
  private char zzBuffer[] = new char[Math.min(ZZ_BUFFERSIZE, zzMaxBufferLen())];

  /** Text position at the last accepting state. */
  private int zzMarkedPos;

  /** Current text position in the buffer. */
  private int zzCurrentPos;

  /** Marks the beginning of the {@link #yytext()} string in the buffer. */
  private int zzStartRead;

  /** Marks the last character in the buffer, that has been read from input. */
  private int zzEndRead;

  /**
   * Whether the scanner is at the end of file.
   * @see #yyatEOF
   */
  private boolean zzAtEOF;

  /**
   * The number of occupied positions in {@link #zzBuffer} beyond {@link #zzEndRead}.
   *
   * <p>When a lead/high surrogate has been read from the input stream into the final
   * {@link #zzBuffer} position, this will have a value of 1; otherwise, it will have a value of 0.
   */
  private int zzFinalHighSurrogate = 0;

  /** For the backwards DFA of general lookahead statements */
  private boolean [] zzFin = new boolean [Math.min(ZZ_BUFFERSIZE, zzMaxBufferLen())+1];

  /** Number of newlines encountered up to the start of the matched text. */
  private int yyline;

  /** Number of characters from the last newline up to the start of the matched text. */
  private int yycolumn;

  /** Number of characters up to the start of the matched text. */
  private long yychar;

  /** Whether the scanner is currently at the beginning of a line. */
  @SuppressWarnings("unused")
  private boolean zzAtBOL = true;

  /** Whether the user-EOF-code has already been executed. */
  @SuppressWarnings("unused")
  private boolean zzEOFDone;

  /* user code: */
	
	Pre() { }



  /**
   * Creates a new scanner
   *
   * @param   in  the java.io.Reader to read input from.
   */
  public Pre(java.io.Reader in) {
    this.zzReader = in;
  }


  /** Returns the maximum size of the scanner buffer, which limits the size of tokens. */
  private int zzMaxBufferLen() {
    return Integer.MAX_VALUE;
  }

  /**  Whether the scanner buffer can grow to accommodate a larger token. */
  private boolean zzCanGrow() {
    return true;
  }

  /**
   * Translates raw input code points to DFA table row
   */
  private static int zzCMap(int input) {
    int offset = input & 255;
    return offset == input ? ZZ_CMAP_BLOCKS[offset] : ZZ_CMAP_BLOCKS[ZZ_CMAP_TOP[input >> 8] | offset];
  }

  /**
   * Refills the input buffer.
   *
   * @return {@code false} iff there was new input.
   * @exception java.io.IOException  if any I/O-Error occurs
   */
  private boolean zzRefill() throws java.io.IOException {

    /* first: make room (if you can) */
    if (zzStartRead > 0) {
      zzEndRead += zzFinalHighSurrogate;
      zzFinalHighSurrogate = 0;
      System.arraycopy(zzBuffer, zzStartRead,
                       zzBuffer, 0,
                       zzEndRead - zzStartRead);

      /* translate stored positions */
      zzEndRead -= zzStartRead;
      zzCurrentPos -= zzStartRead;
      zzMarkedPos -= zzStartRead;
      zzStartRead = 0;
    }

    /* is the buffer big enough? */
    if (zzCurrentPos >= zzBuffer.length - zzFinalHighSurrogate && zzCanGrow()) {
      /* if not, and it can grow: blow it up */
      char newBuffer[] = new char[Math.min(zzBuffer.length * 2, zzMaxBufferLen())];
      System.arraycopy(zzBuffer, 0, newBuffer, 0, zzBuffer.length);
      zzBuffer = newBuffer;
      zzEndRead += zzFinalHighSurrogate;
      zzFinalHighSurrogate = 0;
    }

    /* fill the buffer with new input */
    int requested = zzBuffer.length - zzEndRead;
    int numRead = zzReader.read(zzBuffer, zzEndRead, requested);

    /* not supposed to occur according to specification of java.io.Reader */
    if (numRead == 0) {
      if (requested == 0) {
        throw new java.io.EOFException("Scan buffer limit reached ["+zzBuffer.length+"]");
      }
      else {
        throw new java.io.IOException(
            "Reader returned 0 characters. See JFlex examples/zero-reader for a workaround.");
      }
    }
    if (numRead > 0) {
      zzEndRead += numRead;
      if (Character.isHighSurrogate(zzBuffer[zzEndRead - 1])) {
        if (numRead == requested) { // We requested too few chars to encode a full Unicode character
          --zzEndRead;
          zzFinalHighSurrogate = 1;
        } else {                    // There is room in the buffer for at least one more char
          int c = zzReader.read();  // Expecting to read a paired low surrogate char
          if (c == -1) {
            return true;
          } else {
            zzBuffer[zzEndRead++] = (char)c;
          }
        }
      }
      /* potentially more input available */
      return false;
    }

    /* numRead < 0 ==> end of stream */
    return true;
  }


  /**
   * Closes the input reader.
   *
   * @throws java.io.IOException if the reader could not be closed.
   */
  public final void yyclose() throws java.io.IOException {
    zzAtEOF = true; // indicate end of file
    zzEndRead = zzStartRead; // invalidate buffer

    if (zzReader != null) {
      zzReader.close();
    }
  }


  /**
   * Resets the scanner to read from a new input stream.
   *
   * <p>Does not close the old reader.
   *
   * <p>All internal variables are reset, the old input stream <b>cannot</b> be reused (internal
   * buffer is discarded and lost). Lexical state is set to {@code ZZ_INITIAL}.
   *
   * <p>Internal scan buffer is resized down to its initial length, if it has grown.
   *
   * @param reader The new input stream.
   */
  public final void yyreset(java.io.Reader reader) {
    zzReader = reader;
    zzEOFDone = false;
    yyResetPosition();
    zzLexicalState = YYINITIAL;
    int initBufferSize = Math.min(ZZ_BUFFERSIZE, zzMaxBufferLen());
    if (zzBuffer.length > initBufferSize) {
      zzBuffer = new char[initBufferSize];
    }
  }

  /**
   * Resets the input position.
   */
  private final void yyResetPosition() {
      zzAtBOL  = true;
      zzAtEOF  = false;
      zzCurrentPos = 0;
      zzMarkedPos = 0;
      zzStartRead = 0;
      zzEndRead = 0;
      zzFinalHighSurrogate = 0;
      yyline = 0;
      yycolumn = 0;
      yychar = 0L;
  }


  /**
   * Returns whether the scanner has reached the end of the reader it reads from.
   *
   * @return whether the scanner has reached EOF.
   */
  public final boolean yyatEOF() {
    return zzAtEOF;
  }


  /**
   * Returns the current lexical state.
   *
   * @return the current lexical state.
   */
  public final int yystate() {
    return zzLexicalState;
  }


  /**
   * Enters a new lexical state.
   *
   * @param newState the new lexical state
   */
  public final void yybegin(int newState) {
    zzLexicalState = newState;
  }


  /**
   * Returns the text matched by the current regular expression.
   *
   * @return the matched text.
   */
  public final String yytext() {
    return new String(zzBuffer, zzStartRead, zzMarkedPos-zzStartRead);
  }


  /**
   * Returns the character at the given position from the matched text.
   *
   * <p>It is equivalent to {@code yytext().charAt(pos)}, but faster.
   *
   * @param position the position of the character to fetch. A value from 0 to {@code yylength()-1}.
   *
   * @return the character at {@code position}.
   */
  public final char yycharat(int position) {
    return zzBuffer[zzStartRead + position];
  }


  /**
   * How many characters were matched.
   *
   * @return the length of the matched text region.
   */
  public final int yylength() {
    return zzMarkedPos-zzStartRead;
  }


  /**
   * Reports an error that occurred while scanning.
   *
   * <p>In a well-formed scanner (no or only correct usage of {@code yypushback(int)} and a
   * match-all fallback rule) this method will only be called with things that
   * "Can't Possibly Happen".
   *
   * <p>If this method is called, something is seriously wrong (e.g. a JFlex bug producing a faulty
   * scanner etc.).
   *
   * <p>Usual syntax/scanner level error handling should be done in error fallback rules.
   *
   * @param errorCode the code of the error message to display.
   */
  private static void zzScanError(int errorCode) {
    String message;
    try {
      message = ZZ_ERROR_MSG[errorCode];
    } catch (ArrayIndexOutOfBoundsException e) {
      message = ZZ_ERROR_MSG[ZZ_UNKNOWN_ERROR];
    }

    throw new Error(message);
  }


  /**
   * Pushes the specified amount of characters back into the input stream.
   *
   * <p>They will be read again by then next call of the scanning method.
   *
   * @param number the number of characters to be read again. This number must not be greater than
   *     {@link #yylength()}.
   */
  public void yypushback(int number)  {
    if ( number > yylength() )
      zzScanError(ZZ_PUSHBACK_2BIG);

    zzMarkedPos -= number;
  }




  /**
   * Resumes scanning until the next regular expression is matched, the end of input is encountered
   * or an I/O-Error occurs.
   *
   * @return the next token.
   * @exception java.io.IOException if any I/O-Error occurs.
   */
  public int yylex() throws java.io.IOException
  {
    int zzInput;
    int zzAction;

    // cached fields:
    int zzCurrentPosL;
    int zzMarkedPosL;
    int zzEndReadL = zzEndRead;
    char[] zzBufferL = zzBuffer;

    int [] zzTransL = ZZ_TRANS;
    int [] zzRowMapL = ZZ_ROWMAP;
    int [] zzAttrL = ZZ_ATTRIBUTE;

    while (true) {
      zzMarkedPosL = zzMarkedPos;

      yychar+= zzMarkedPosL-zzStartRead;

      boolean zzR = false;
      int zzCh;
      int zzCharCount;
      for (zzCurrentPosL = zzStartRead  ;
           zzCurrentPosL < zzMarkedPosL ;
           zzCurrentPosL += zzCharCount ) {
        zzCh = Character.codePointAt(zzBufferL, zzCurrentPosL, zzMarkedPosL);
        zzCharCount = Character.charCount(zzCh);
        switch (zzCh) {
        case '\u000B':  // fall through
        case '\u000C':  // fall through
        case '\u0085':  // fall through
        case '\u2028':  // fall through
        case '\u2029':
          yyline++;
          yycolumn = 0;
          zzR = false;
          break;
        case '\r':
          yyline++;
          yycolumn = 0;
          zzR = true;
          break;
        case '\n':
          if (zzR)
            zzR = false;
          else {
            yyline++;
            yycolumn = 0;
          }
          break;
        default:
          zzR = false;
          yycolumn += zzCharCount;
        }
      }

      if (zzR) {
        // peek one character ahead if it is
        // (if we have counted one line too much)
        boolean zzPeek;
        if (zzMarkedPosL < zzEndReadL)
          zzPeek = zzBufferL[zzMarkedPosL] == '\n';
        else if (zzAtEOF)
          zzPeek = false;
        else {
          boolean eof = zzRefill();
          zzEndReadL = zzEndRead;
          zzMarkedPosL = zzMarkedPos;
          zzBufferL = zzBuffer;
          if (eof)
            zzPeek = false;
          else
            zzPeek = zzBufferL[zzMarkedPosL] == '\n';
        }
        if (zzPeek) yyline--;
      }
      zzAction = -1;

      zzCurrentPosL = zzCurrentPos = zzStartRead = zzMarkedPosL;

      zzState = ZZ_LEXSTATE[zzLexicalState];

      // set up zzAction for empty match case:
      int zzAttributes = zzAttrL[zzState];
      if ( (zzAttributes & 1) == 1 ) {
        zzAction = zzState;
      }


      zzForAction: {
        while (true) {

          if (zzCurrentPosL < zzEndReadL) {
            zzInput = Character.codePointAt(zzBufferL, zzCurrentPosL, zzEndReadL);
            zzCurrentPosL += Character.charCount(zzInput);
          }
          else if (zzAtEOF) {
            zzInput = YYEOF;
            break zzForAction;
          }
          else {
            // store back cached positions
            zzCurrentPos  = zzCurrentPosL;
            zzMarkedPos   = zzMarkedPosL;
            boolean eof = zzRefill();
            // get translated positions and possibly new buffer
            zzCurrentPosL  = zzCurrentPos;
            zzMarkedPosL   = zzMarkedPos;
            zzBufferL      = zzBuffer;
            zzEndReadL     = zzEndRead;
            if (eof) {
              zzInput = YYEOF;
              break zzForAction;
            }
            else {
              zzInput = Character.codePointAt(zzBufferL, zzCurrentPosL, zzEndReadL);
              zzCurrentPosL += Character.charCount(zzInput);
            }
          }
          int zzNext = zzTransL[ zzRowMapL[zzState] + zzCMap(zzInput) ];
          if (zzNext == -1) break zzForAction;
          zzState = zzNext;

          zzAttributes = zzAttrL[zzState];
          if ( (zzAttributes & 1) == 1 ) {
            zzAction = zzState;
            zzMarkedPosL = zzCurrentPosL;
            if ( (zzAttributes & 8) == 8 ) break zzForAction;
          }

        }
      }

      // store back cached position
      zzMarkedPos = zzMarkedPosL;

      if (zzInput == YYEOF && zzStartRead == zzCurrentPos) {
        zzAtEOF = true;
            switch (zzLexicalState) {
            case inMrkPre: {
              if(getMarkerDocument().isDebug()) System.out.println("Annotazioni [MRK:PRE] nel testo in input non bilanciate");
	
		getMarkerDocument().addError("Annotazioni [MRK_SKIP] nel testo in input non bilanciate");
		
		output.append("</mrk:pre>");
		
		return YYEOF;
            }  // fall though
            case 161: break;
            default:
        return YYEOF;
        }
      }
      else {
        switch (zzAction < 0 ? zzAction : ZZ_ACTION[zzAction]) {
          case 1:
            { first = false;

	output.append(yytext());
            }
          // fall through
          case 15: break;
          case 2:
            { if( !first) output.append("\n");
            }
          // fall through
          case 16: break;
          case 3:
            { if( !first) output.append(" ");
            }
          // fall through
          case 17: break;
          case 4:
            { output.append("&amp;");
            }
          // fall through
          case 18: break;
          case 5:
            { output.append("&lt;");
            }
          // fall through
          case 19: break;
          case 6:
            { output.append("&gt;");
            }
          // fall through
          case 20: break;
          case 7:
            { output.append(" ");
            }
          // fall through
          case 21: break;
          case 8:
            { output.append("-");
            }
          // fall through
          case 22: break;
          case 9:
            { output.append(yytext());
            }
          // fall through
          case 23: break;
          case 10:
            // general lookahead, find correct zzMarkedPos
            { int zzFState = 2;
              int zzFPos = zzStartRead;
              if (zzFin.length <= zzBufferL.length) {
                zzFin = new boolean[zzBufferL.length+1];
              }
              boolean zzFinL[] = zzFin;
              while (zzFState != -1 && zzFPos < zzMarkedPos) {
                zzFinL[zzFPos] = ((zzAttrL[zzFState] & 1) == 1);
                zzInput = Character.codePointAt(zzBufferL, zzFPos, zzMarkedPos);
                zzFPos += Character.charCount(zzInput);
                zzFState = zzTransL[ zzRowMapL[zzFState] + zzCMap(zzInput) ];
              }
              if (zzFState != -1) {
                zzFinL[zzFPos++] = ((zzAttrL[zzFState] & 1) == 1);
              }
              while (zzFPos <= zzMarkedPos) {
                zzFinL[zzFPos++] = false;
              }

              zzFState = 3;
              zzFPos = zzMarkedPos;
              while (!zzFinL[zzFPos] || (zzAttrL[zzFState] & 1) != 1) {
                zzInput = Character.codePointBefore(zzBufferL, zzFPos, zzStartRead);
                zzFPos -= Character.charCount(zzInput);
                zzFState = zzTransL[ zzRowMapL[zzFState] + zzCMap(zzInput) ];
              };
              zzMarkedPos = zzFPos;
            }
            { if( !first) output.append(yytext());
            }
          // fall through
          case 24: break;
          case 11:
            { if(first) {
	
		first = false;
		
		if(getMarkerDocument().isDebug()) System.out.println("Rilevata intestazione di atto comunitario - set inputType PLAIN_EUR_LEX");
		
		getMarkerDocument().setInputType(InputType.PLAIN_EUR_LEX);
	}
	
	output.append(yytext());
            }
          // fall through
          case 25: break;
          case 12:
            { output.append("<h:pre>"); yybegin(inMrkPre);
            }
          // fall through
          case 26: break;
          case 13:
            { if(getMarkerDocument().isDebug()) System.out.println("Annotazioni [MRK:PRE] nel testo in input non bilanciate");
	
		getMarkerDocument().addError("Annotazioni [MRK:PRE] nel testo in input non bilanciate");
            }
          // fall through
          case 27: break;
          case 14:
            { output.append("</h:pre>"); yybegin(0);
            }
          // fall through
          case 28: break;
          default:
            zzScanError(ZZ_NO_MATCH);
        }
      }
    }
  }

  /**
   * Runs the scanner on input files.
   *
   * This is a standalone scanner, it will print any unmatched
   * text to System.out unchanged.
   *
   * @param argv   the command line, contains the filenames to run
   *               the scanner on.
   */
  public static void main(String[] argv) {
    if (argv.length == 0) {
      System.out.println("Usage : java Pre [ --encoding <name> ] <inputfile(s)>");
    }
    else {
      int firstFilePos = 0;
      String encodingName = "UTF-8";
      if (argv[0].equals("--encoding")) {
        firstFilePos = 2;
        encodingName = argv[1];
        try {
          // Side-effect: is encodingName valid?
          java.nio.charset.Charset.forName(encodingName);
        } catch (Exception e) {
          System.out.println("Invalid encoding '" + encodingName + "'");
          return;
        }
      }
      for (int i = firstFilePos; i < argv.length; i++) {
        Pre scanner = null;
        java.io.FileInputStream stream = null;
        java.io.Reader reader = null;
        try {
          stream = new java.io.FileInputStream(argv[i]);
          reader = new java.io.InputStreamReader(stream, encodingName);
          scanner = new Pre(reader);
          while ( !scanner.zzAtEOF ) scanner.yylex();
        }
        catch (java.io.FileNotFoundException e) {
          System.out.println("File not found : \""+argv[i]+"\"");
        }
        catch (java.io.IOException e) {
          System.out.println("IO error scanning file \""+argv[i]+"\"");
          System.out.println(e);
        }
        catch (Exception e) {
          System.out.println("Unexpected exception:");
          e.printStackTrace();
        }
        finally {
          if (reader != null) {
            try {
              reader.close();
            }
            catch (java.io.IOException e) {
              System.out.println("IO error closing file \""+argv[i]+"\"");
              System.out.println(e);
            }
          }
          if (stream != null) {
            try {
              stream.close();
            }
            catch (java.io.IOException e) {
              System.out.println("IO error closing file \""+argv[i]+"\"");
              System.out.println(e);
            }
          }
        }
      }
    }
  }


}
