// DO NOT EDIT
// Generated by JFlex 1.9.1 http://jflex.de/
// source: jflex/ReadPart.jflex

/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.marker;

import java.io.IOException;
import java.io.StringReader;


@SuppressWarnings("fallthrough")
public class ReadPart extends JFlexAnnotationService {

  /** This character denotes the end of file. */
  public static final int YYEOF = -1;

  /** Initial size of the lookahead buffer. */
  private static final int ZZ_BUFFERSIZE = 16384;

  // Lexical states.
  public static final int YYINITIAL = 0;
  public static final int inArticleHeader = 2;
  public static final int inArticleTitle = 4;
  public static final int inCommaHeader = 6;
  public static final int inArticle = 8;
  public static final int inComma = 10;
  public static final int inCommaOpen = 12;
  public static final int inParagraph = 14;
  public static final int inParagraphOpen = 16;

  /**
   * ZZ_LEXSTATE[l] is the state in the DFA for the lexical state l
   * ZZ_LEXSTATE[l+1] is the state in the DFA for the lexical state l
   *                  at the beginning of a line
   * l is of the form l = 2*k, k a non negative integer
   */
  private static final int ZZ_LEXSTATE[] = {
     0,  0,  1,  1,  2,  2,  3,  3,  4,  4,  5,  5,  6,  6,  7,  7, 
     8, 8
  };

  /**
   * Top-level table for translating characters to character classes
   */
  private static final int [] ZZ_CMAP_TOP = zzUnpackcmap_top();

  private static final String ZZ_CMAP_TOP_PACKED_0 =
    "\1\0\1\u0100\36\u0200\1\u0300\1\u0400\u10de\u0200";

  private static int [] zzUnpackcmap_top() {
    int [] result = new int[4352];
    int offset = 0;
    offset = zzUnpackcmap_top(ZZ_CMAP_TOP_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackcmap_top(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }


  /**
   * Second-level tables for translating characters to character classes
   */
  private static final int [] ZZ_CMAP_BLOCKS = zzUnpackcmap_blocks();

  private static final String ZZ_CMAP_BLOCKS_PACKED_0 =
    "\11\0\1\1\2\0\1\1\23\0\1\1\5\0\1\2"+
    "\10\0\1\3\12\0\1\4\1\5\1\6\1\0\1\7"+
    "\2\0\1\10\1\11\1\12\1\0\1\13\1\14\1\15"+
    "\1\0\1\16\1\0\1\17\1\20\1\21\1\22\1\23"+
    "\1\24\1\0\1\25\1\26\1\27\1\30\4\0\1\31"+
    "\6\0\1\10\1\11\1\12\1\0\1\13\1\14\1\15"+
    "\1\0\1\16\1\0\1\17\1\20\1\21\1\22\1\23"+
    "\1\24\1\0\1\25\1\26\1\27\1\30\4\0\1\31"+
    "\265\0\2\16\115\0\1\26\u0182\0\1\1\u0127\0\1\17"+
    "\325\0";

  private static int [] zzUnpackcmap_blocks() {
    int [] result = new int[1280];
    int offset = 0;
    offset = zzUnpackcmap_blocks(ZZ_CMAP_BLOCKS_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackcmap_blocks(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }

  /**
   * Translates DFA states to action switch labels.
   */
  private static final int [] ZZ_ACTION = zzUnpackAction();

  private static final String ZZ_ACTION_PACKED_0 =
    "\11\0\2\1\4\2\2\3\1\2\1\3\1\2\1\3"+
    "\226\0\1\4\7\0\1\5\6\0\1\6\26\0\1\7"+
    "\2\0\1\10\14\0\1\11\12\0\1\12\13\0\1\13"+
    "\4\0\1\14\3\0\1\15\5\0\1\16\3\0\1\17"+
    "\1\0\1\20\1\21\1\22\5\0\1\23\1\24\1\25";

  private static int [] zzUnpackAction() {
    int [] result = new int[280];
    int offset = 0;
    offset = zzUnpackAction(ZZ_ACTION_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackAction(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }


  /**
   * Translates a state to a row index in the transition table
   */
  private static final int [] ZZ_ROWMAP = zzUnpackRowMap();

  private static final String ZZ_ROWMAP_PACKED_0 =
    "\0\0\0\32\0\64\0\116\0\150\0\202\0\234\0\266"+
    "\0\320\0\352\0\u0104\0\352\0\u011e\0\u0138\0\u0152\0\352"+
    "\0\u016c\0\u0186\0\u01a0\0\u01ba\0\u01d4\0\u01ee\0\u0208\0\u0222"+
    "\0\u023c\0\u0256\0\u0270\0\u028a\0\u02a4\0\u02be\0\u02d8\0\u02f2"+
    "\0\u030c\0\u0326\0\u0340\0\u035a\0\u0374\0\u038e\0\u03a8\0\u03c2"+
    "\0\u03dc\0\u03f6\0\u0410\0\u042a\0\u0444\0\u045e\0\u0478\0\u0492"+
    "\0\u04ac\0\u04c6\0\u04e0\0\u04fa\0\u0514\0\u052e\0\u0548\0\u0562"+
    "\0\u057c\0\u0596\0\u05b0\0\u05ca\0\u05e4\0\u05fe\0\u0618\0\u0632"+
    "\0\u064c\0\u0666\0\u0680\0\u069a\0\u06b4\0\u06ce\0\u06e8\0\u0702"+
    "\0\u071c\0\u0736\0\u0750\0\u076a\0\u0784\0\u079e\0\u07b8\0\u07d2"+
    "\0\u07ec\0\u0806\0\u0820\0\u083a\0\u0854\0\u086e\0\u0888\0\u08a2"+
    "\0\u08bc\0\u08d6\0\u08f0\0\u090a\0\u0924\0\u093e\0\u0958\0\u0972"+
    "\0\u098c\0\u09a6\0\u09c0\0\u09da\0\u09f4\0\u0a0e\0\u0a28\0\u0a42"+
    "\0\u0a5c\0\u0a76\0\u0a90\0\u0aaa\0\u0ac4\0\u0ade\0\u0af8\0\u0b12"+
    "\0\u0b2c\0\u0b46\0\u0b60\0\u0b7a\0\u0b94\0\u0bae\0\u0bc8\0\u0be2"+
    "\0\u0bfc\0\u0c16\0\u0c30\0\u0c4a\0\u0c64\0\u0c7e\0\u0c98\0\u0cb2"+
    "\0\u0ccc\0\u0ce6\0\u0d00\0\u0d1a\0\u0d34\0\u0d4e\0\u0d68\0\u0d82"+
    "\0\u0d9c\0\u0db6\0\u0dd0\0\u0dea\0\u0e04\0\u0e1e\0\u0e38\0\u0e52"+
    "\0\u0e6c\0\u0e86\0\u0ea0\0\u0eba\0\u0ed4\0\u0eee\0\u0f08\0\u0f22"+
    "\0\u0f3c\0\u0f56\0\u0f70\0\u0f8a\0\u0fa4\0\u0fbe\0\u0fd8\0\u0ff2"+
    "\0\u100c\0\u1026\0\u1040\0\u105a\0\u1074\0\u108e\0\u10a8\0\u10c2"+
    "\0\u10dc\0\u10f6\0\u1110\0\352\0\u112a\0\u1144\0\u115e\0\u1178"+
    "\0\u1192\0\u11ac\0\u11c6\0\352\0\u11e0\0\u11fa\0\u1214\0\u122e"+
    "\0\u1248\0\u1262\0\352\0\u127c\0\u1296\0\u12b0\0\u12ca\0\u12e4"+
    "\0\u12fe\0\u1318\0\u1332\0\u134c\0\u1366\0\u1380\0\u139a\0\u13b4"+
    "\0\u13ce\0\u13e8\0\u1402\0\u141c\0\u1436\0\u1450\0\u146a\0\u1484"+
    "\0\u149e\0\352\0\u14b8\0\u14d2\0\352\0\u14ec\0\u1506\0\u1520"+
    "\0\u153a\0\u1554\0\u156e\0\u1588\0\u15a2\0\u15bc\0\u15d6\0\u15f0"+
    "\0\u160a\0\352\0\u1624\0\u163e\0\u1658\0\u1672\0\u168c\0\u16a6"+
    "\0\u16c0\0\u16da\0\u16f4\0\u170e\0\352\0\u1728\0\u1742\0\u175c"+
    "\0\u1776\0\u1790\0\u17aa\0\u17c4\0\u17de\0\u17f8\0\u1812\0\u182c"+
    "\0\352\0\u1846\0\u1860\0\u187a\0\u1894\0\352\0\u18ae\0\u18c8"+
    "\0\u18e2\0\352\0\u18fc\0\u1916\0\u1930\0\u194a\0\u1964\0\352"+
    "\0\u197e\0\u1998\0\u19b2\0\352\0\u19cc\0\352\0\352\0\352"+
    "\0\u19e6\0\u1a00\0\u1a1a\0\u1a34\0\u1a4e\0\352\0\352\0\352";

  private static int [] zzUnpackRowMap() {
    int [] result = new int[280];
    int offset = 0;
    offset = zzUnpackRowMap(ZZ_ROWMAP_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackRowMap(String packed, int offset, int [] result) {
    int i = 0;  /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length() - 1;
    while (i < l) {
      int high = packed.charAt(i++) << 16;
      result[j++] = high | packed.charAt(i++);
    }
    return j;
  }

  /**
   * The transition table of the DFA
   */
  private static final int [] ZZ_TRANS = zzUnpacktrans();

  private static final String ZZ_TRANS_PACKED_0 =
    "\6\12\1\13\23\12\6\14\1\15\31\14\1\16\31\14"+
    "\1\17\23\14\6\20\1\21\23\20\6\14\1\22\23\14"+
    "\6\20\1\23\23\20\6\14\1\24\23\14\6\20\1\25"+
    "\23\20\53\0\1\26\13\0\1\27\31\0\1\30\31\0"+
    "\1\31\31\0\1\32\15\0\1\33\13\0\1\34\15\0"+
    "\1\35\13\0\1\36\15\0\1\37\13\0\1\40\15\0"+
    "\1\41\13\0\1\42\53\0\1\43\25\0\1\44\31\0"+
    "\1\45\31\0\1\46\31\0\1\47\35\0\1\50\25\0"+
    "\1\51\35\0\1\52\25\0\1\53\35\0\1\54\25\0"+
    "\1\55\35\0\1\56\25\0\1\57\27\0\1\60\37\0"+
    "\1\61\31\0\1\62\31\0\1\63\31\0\1\64\23\0"+
    "\1\65\37\0\1\66\23\0\1\67\37\0\1\70\23\0"+
    "\1\71\37\0\1\72\23\0\1\73\37\0\1\74\10\0"+
    "\1\75\44\0\1\76\31\0\1\77\31\0\1\100\31\0"+
    "\1\101\16\0\1\102\44\0\1\103\16\0\1\104\44\0"+
    "\1\105\16\0\1\106\44\0\1\107\16\0\1\110\44\0"+
    "\1\111\22\0\1\112\1\0\1\113\11\0\1\114\11\0"+
    "\1\115\31\0\1\116\31\0\1\117\31\0\1\120\37\0"+
    "\1\121\3\0\1\122\5\0\1\123\1\124\10\0\1\125"+
    "\37\0\1\126\23\0\1\127\43\0\1\130\5\0\1\131"+
    "\11\0\1\132\51\0\1\133\11\0\1\134\52\0\1\135"+
    "\27\0\1\136\16\0\1\137\37\0\1\140\40\0\1\141"+
    "\22\0\1\142\23\0\1\143\44\0\1\144\30\0\1\145"+
    "\17\0\1\146\51\0\1\147\11\0\1\150\44\0\1\151"+
    "\20\0\1\152\41\0\1\153\17\0\1\154\33\0\1\155"+
    "\27\0\1\156\45\0\1\157\34\0\1\160\23\0\1\161"+
    "\35\0\1\162\26\0\1\163\37\0\1\164\23\0\1\165"+
    "\34\0\1\166\25\0\1\167\37\0\1\170\27\0\1\171"+
    "\15\0\1\172\45\0\1\173\25\0\1\174\33\0\1\175"+
    "\35\0\1\176\27\0\1\177\27\0\1\200\33\0\1\201"+
    "\14\0\1\202\37\0\1\203\34\0\1\204\20\0\1\205"+
    "\50\0\1\206\13\0\1\207\47\0\1\210\31\0\1\211"+
    "\23\0\1\212\23\0\1\213\26\0\1\214\46\0\1\215"+
    "\33\0\1\216\23\0\1\217\31\0\1\220\23\0\1\221"+
    "\26\0\1\222\42\0\1\223\20\0\1\224\46\0\1\225"+
    "\16\0\1\226\27\0\1\227\36\0\1\230\27\0\1\231"+
    "\43\0\1\232\17\0\1\233\34\0\1\234\23\0\1\235"+
    "\47\0\1\236\20\0\1\237\32\0\1\240\31\0\1\241"+
    "\23\0\1\242\42\0\1\243\36\0\1\244\20\0\1\245"+
    "\35\0\1\246\25\0\1\247\24\0\1\250\44\0\1\251"+
    "\7\0\1\252\1\253\4\0\1\254\47\0\1\255\32\0"+
    "\1\256\21\0\1\257\41\0\1\260\15\0\1\261\20\0"+
    "\1\262\1\263\4\0\1\264\51\0\1\265\27\0\1\266"+
    "\16\0\1\267\31\0\1\270\20\0\1\271\1\272\4\0"+
    "\1\273\32\0\1\274\50\0\1\275\27\0\1\276\14\0"+
    "\1\277\46\0\1\300\21\0\1\301\34\0\1\302\11\0"+
    "\7\303\1\0\22\303\22\0\1\304\17\0\1\305\50\0"+
    "\1\306\14\0\1\307\46\0\1\310\25\0\1\311\6\0"+
    "\7\312\1\0\22\312\22\0\1\313\17\0\1\314\31\0"+
    "\1\315\31\0\1\316\44\0\1\317\6\0\7\320\1\0"+
    "\22\320\22\0\1\321\16\0\1\322\32\0\1\323\31\0"+
    "\1\324\30\0\1\325\32\0\1\326\46\0\1\327\27\0"+
    "\1\330\6\0\7\303\1\254\22\303\11\0\1\331\34\0"+
    "\1\332\25\0\1\333\31\0\1\334\31\0\1\335\41\0"+
    "\1\336\11\0\7\312\1\264\22\312\11\0\1\337\51\0"+
    "\1\340\14\0\1\341\24\0\1\342\42\0\1\343\11\0"+
    "\7\320\1\273\22\320\11\0\1\344\51\0\1\345\14\0"+
    "\1\346\31\0\1\347\25\0\1\350\21\0\7\351\1\0"+
    "\22\351\26\0\1\352\26\0\1\353\37\0\1\354\7\0"+
    "\1\355\53\0\1\356\23\0\1\357\34\0\1\360\21\0"+
    "\1\361\36\0\1\362\31\0\1\363\34\0\1\364\21\0"+
    "\1\365\36\0\1\366\31\0\1\367\22\0\1\370\15\0"+
    "\7\351\1\371\22\351\24\0\1\372\5\0\7\373\1\0"+
    "\22\373\16\0\1\374\31\0\1\375\22\0\1\376\46\0"+
    "\1\377\30\0\1\u0100\6\0\7\u0101\1\0\22\u0101\7\0"+
    "\1\u0102\46\0\1\u0103\30\0\1\u0104\6\0\7\u0105\1\0"+
    "\22\u0105\7\u0106\1\0\22\u0106\23\0\1\u0107\13\0\1\252"+
    "\24\0\7\373\1\u0108\22\373\23\0\1\u0109\31\0\1\u010a"+
    "\13\0\1\262\46\0\1\u010b\7\0\7\u0101\1\u010c\22\u0101"+
    "\5\0\1\271\46\0\1\u010d\7\0\7\u0105\1\u010e\22\u0105"+
    "\7\u0106\1\u010f\22\u0106\7\0\1\u0110\44\0\1\u0111\31\0"+
    "\1\u0112\22\0\1\u0113\31\0\1\u0114\31\0\1\334\31\0"+
    "\1\u0115\25\0\1\u0116\31\0\1\u0117\31\0\1\u0118\22\0";

  private static int [] zzUnpacktrans() {
    int [] result = new int[6760];
    int offset = 0;
    offset = zzUnpacktrans(ZZ_TRANS_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpacktrans(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      value--;
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }


  /** Error code for "Unknown internal scanner error". */
  private static final int ZZ_UNKNOWN_ERROR = 0;
  /** Error code for "could not match input". */
  private static final int ZZ_NO_MATCH = 1;
  /** Error code for "pushback value was too large". */
  private static final int ZZ_PUSHBACK_2BIG = 2;

  /**
   * Error messages for {@link #ZZ_UNKNOWN_ERROR}, {@link #ZZ_NO_MATCH}, and
   * {@link #ZZ_PUSHBACK_2BIG} respectively.
   */
  private static final String ZZ_ERROR_MSG[] = {
    "Unknown internal scanner error",
    "Error: could not match input",
    "Error: pushback value was too large"
  };

  /**
   * ZZ_ATTRIBUTE[aState] contains the attributes of state {@code aState}
   */
  private static final int [] ZZ_ATTRIBUTE = zzUnpackAttribute();

  private static final String ZZ_ATTRIBUTE_PACKED_0 =
    "\11\0\1\11\1\1\1\11\3\1\1\11\5\1\226\0"+
    "\1\11\7\0\1\11\6\0\1\11\26\0\1\11\2\0"+
    "\1\11\14\0\1\11\12\0\1\11\13\0\1\11\4\0"+
    "\1\11\3\0\1\11\5\0\1\11\3\0\1\11\1\0"+
    "\3\11\5\0\3\11";

  private static int [] zzUnpackAttribute() {
    int [] result = new int[280];
    int offset = 0;
    offset = zzUnpackAttribute(ZZ_ATTRIBUTE_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackAttribute(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }

  /** Input device. */
  private java.io.Reader zzReader;

  /** Current state of the DFA. */
  private int zzState;

  /** Current lexical state. */
  private int zzLexicalState = YYINITIAL;

  /**
   * This buffer contains the current text to be matched and is the source of the {@link #yytext()}
   * string.
   */
  private char zzBuffer[] = new char[Math.min(ZZ_BUFFERSIZE, zzMaxBufferLen())];

  /** Text position at the last accepting state. */
  private int zzMarkedPos;

  /** Current text position in the buffer. */
  private int zzCurrentPos;

  /** Marks the beginning of the {@link #yytext()} string in the buffer. */
  private int zzStartRead;

  /** Marks the last character in the buffer, that has been read from input. */
  private int zzEndRead;

  /**
   * Whether the scanner is at the end of file.
   * @see #yyatEOF
   */
  private boolean zzAtEOF;

  /**
   * The number of occupied positions in {@link #zzBuffer} beyond {@link #zzEndRead}.
   *
   * <p>When a lead/high surrogate has been read from the input stream into the final
   * {@link #zzBuffer} position, this will have a value of 1; otherwise, it will have a value of 0.
   */
  private int zzFinalHighSurrogate = 0;

  /** Number of newlines encountered up to the start of the matched text. */
  private int yyline;

  /** Number of characters from the last newline up to the start of the matched text. */
  private int yycolumn;

  /** Number of characters up to the start of the matched text. */
  private long yychar;

  /** Whether the scanner is currently at the beginning of a line. */
  @SuppressWarnings("unused")
  private boolean zzAtBOL = true;

  /** Whether the user-EOF-code has already been executed. */
  @SuppressWarnings("unused")
  private boolean zzEOFDone;

  /* user code: */
	
	ReadPart() { }
	
	private StringBuilder text = new StringBuilder(); //buffer da inviare a Part per l'analisi

	private void runService(String inputText) {
	
		String containerId = getMarkerDocument().getContext().getContainerId();
			
		String serviceName = getMarkerDocument().getContext().getCurrentPartType();
				
		if(getMarkerDocument().isDebug()) System.out.println("RUN " + serviceName + " ON " + containerId + " :\n" + inputText);
		
		if(serviceName.equals("checkarticolo")) {
			
			//if(containerId.indexOf("article") == -1) {
			if(containerId.indexOf("articolo") == -1) {
				
				//do nothing, non siamo in un articolo
				return;
			}
			
			//System.out.println("RUN " + serviceName + " ON:\n" + inputText);
			
			CheckArticolo checkArticolo = new CheckArticolo();
			checkArticolo.setInput(inputText);
			checkArticolo.setMarkerDocument(getMarkerDocument());
			checkArticolo.run();
			
			ArticoloContext articoloContext = new ArticoloContext();
			articoloContext.setId(containerId);
				
			if(checkArticolo.numerato) {
			
				articoloContext.setCommiNumerati(true);
				
				articoloContext.setNothingBeforeComma(checkArticolo.nothingBeforeNumbered);	
			}
			
			if(checkArticolo.rubricaParentesi) {
				
				articoloContext.setFirstParagraphInBrackets(true);
			
			} else {
				
				if(checkArticolo.firstEndsWithColon || checkArticolo.firstNotRubrica) articoloContext.setFirstNotRubrica(true);
				
				if(checkArticolo.meaningfuls < 2) articoloContext.setOneMeaningfulParagraph(true);
			}
			
			articoloContext.setFirstParagraphLength(checkArticolo.firstParagraphLength);

			getMarkerDocument().getContext().addArticoloContext(articoloContext.getId(), articoloContext);
			
			return;
		}		
		
		if(serviceName.equals("checknumlist")) {
			
			//if(containerId.indexOf("article") == -1) {
			if(containerId.indexOf("articolo") == -1) {
				
				//do nothing, non siamo in un articolo
				return;
			}
			
			CheckNumberedItems cn = new CheckNumberedItems();
			cn.setInput(inputText);
			cn.setMarkerDocument(getMarkerDocument());
			cn.run();
			
			output.append(cn.getOutput());
			
			return;
		}		
		
		if(serviceName.equals("checkfirstcomma")) {
			
			//if(containerId.indexOf("article") == -1) {
			if(containerId.indexOf("articolo") == -1) {
				
				//do nothing, non siamo in un articolo
				return;
			}
			
			CheckFirstCommaL cf = new CheckFirstCommaL();
			cf.setInput(inputText);
			cf.setMarkerDocument(getMarkerDocument());
			cf.run();
			
			return;
		}	
	
		//Lancia l'automa selezionato sull'inputText e appendi il suo output
		
		JFlexAnnotationService service = null;

		
		if(getMarkerDocument().getContext().isInQuotes()) {
		
			if(getMarkerDocument().isDebug()) System.out.println("inQuotes RUN " + serviceName + " ON:\n" + inputText);
		}
		
		
		if(serviceName.equals("lista")) {
		
			//System.out.println("RUN LISTA " + serviceName + " ON:\n" + inputText);
			
			service = new List();
			service.setMarkerDocument(getMarkerDocument());
			service.yyreset(new StringReader(inputText));
	
		} else {
		
			if(serviceName.equals("paragrafo") && containerId.indexOf("articolo") == -1) {
				
				//do nothing - non eseguire analisi paragrafi se il container non è articolo o inferiore
				
				output.append(inputText);
				return;
			} 
		
			if(getMarkerDocument().getContext().isInQuotes()) {
			
				service = new PartInQuotes();
				service.setMarkerDocument(getMarkerDocument());
				service.yyreset(new StringReader(inputText));
				((PartInQuotes) service).setInitialState();
				
			} else {
			
				service = new Part();
				service.setMarkerDocument(getMarkerDocument());
				service.yyreset(new StringReader(inputText));
				((Part) service).setInitialState();
			}
		}
		
		try {
		
			service.yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			return;
		}
	
		//System.out.println("OUTPUT OF " + serviceName + ":\n" + service.getOutput());
		
		if( !Marker.checkAnnotations(inputText, service.getOutput(), serviceName, getMarkerDocument().isDebug())) {
		
			String message = "idref:" + getMarkerDocument().getContext().getContainerId() + " controllo di consistenza del testo fallito nel servizio \"" + serviceName + "\""; 
			
			if(getMarkerDocument().isDebug()) System.out.println(message);
			
			/* In questo caso mettilo tra gli Warning e non tra gli Errori: lo scope è limitato e l'errore probabilmente non è catastrofico */
			//getMarkerDocument().addWarning(message);
			/* 27-01-2023 rimesso tra gli errori, può essere molto importante */
			getMarkerDocument().addError(message);
			
			output.append(inputText);
		
		} else {
	
			output.append(service.getOutput());
		}
	}


  /**
   * Creates a new scanner
   *
   * @param   in  the java.io.Reader to read input from.
   */
  public ReadPart(java.io.Reader in) {
    this.zzReader = in;
  }


  /** Returns the maximum size of the scanner buffer, which limits the size of tokens. */
  private int zzMaxBufferLen() {
    return Integer.MAX_VALUE;
  }

  /**  Whether the scanner buffer can grow to accommodate a larger token. */
  private boolean zzCanGrow() {
    return true;
  }

  /**
   * Translates raw input code points to DFA table row
   */
  private static int zzCMap(int input) {
    int offset = input & 255;
    return offset == input ? ZZ_CMAP_BLOCKS[offset] : ZZ_CMAP_BLOCKS[ZZ_CMAP_TOP[input >> 8] | offset];
  }

  /**
   * Refills the input buffer.
   *
   * @return {@code false} iff there was new input.
   * @exception java.io.IOException  if any I/O-Error occurs
   */
  private boolean zzRefill() throws java.io.IOException {

    /* first: make room (if you can) */
    if (zzStartRead > 0) {
      zzEndRead += zzFinalHighSurrogate;
      zzFinalHighSurrogate = 0;
      System.arraycopy(zzBuffer, zzStartRead,
                       zzBuffer, 0,
                       zzEndRead - zzStartRead);

      /* translate stored positions */
      zzEndRead -= zzStartRead;
      zzCurrentPos -= zzStartRead;
      zzMarkedPos -= zzStartRead;
      zzStartRead = 0;
    }

    /* is the buffer big enough? */
    if (zzCurrentPos >= zzBuffer.length - zzFinalHighSurrogate && zzCanGrow()) {
      /* if not, and it can grow: blow it up */
      char newBuffer[] = new char[Math.min(zzBuffer.length * 2, zzMaxBufferLen())];
      System.arraycopy(zzBuffer, 0, newBuffer, 0, zzBuffer.length);
      zzBuffer = newBuffer;
      zzEndRead += zzFinalHighSurrogate;
      zzFinalHighSurrogate = 0;
    }

    /* fill the buffer with new input */
    int requested = zzBuffer.length - zzEndRead;
    int numRead = zzReader.read(zzBuffer, zzEndRead, requested);

    /* not supposed to occur according to specification of java.io.Reader */
    if (numRead == 0) {
      if (requested == 0) {
        throw new java.io.EOFException("Scan buffer limit reached ["+zzBuffer.length+"]");
      }
      else {
        throw new java.io.IOException(
            "Reader returned 0 characters. See JFlex examples/zero-reader for a workaround.");
      }
    }
    if (numRead > 0) {
      zzEndRead += numRead;
      if (Character.isHighSurrogate(zzBuffer[zzEndRead - 1])) {
        if (numRead == requested) { // We requested too few chars to encode a full Unicode character
          --zzEndRead;
          zzFinalHighSurrogate = 1;
        } else {                    // There is room in the buffer for at least one more char
          int c = zzReader.read();  // Expecting to read a paired low surrogate char
          if (c == -1) {
            return true;
          } else {
            zzBuffer[zzEndRead++] = (char)c;
          }
        }
      }
      /* potentially more input available */
      return false;
    }

    /* numRead < 0 ==> end of stream */
    return true;
  }


  /**
   * Closes the input reader.
   *
   * @throws java.io.IOException if the reader could not be closed.
   */
  public final void yyclose() throws java.io.IOException {
    zzAtEOF = true; // indicate end of file
    zzEndRead = zzStartRead; // invalidate buffer

    if (zzReader != null) {
      zzReader.close();
    }
  }


  /**
   * Resets the scanner to read from a new input stream.
   *
   * <p>Does not close the old reader.
   *
   * <p>All internal variables are reset, the old input stream <b>cannot</b> be reused (internal
   * buffer is discarded and lost). Lexical state is set to {@code ZZ_INITIAL}.
   *
   * <p>Internal scan buffer is resized down to its initial length, if it has grown.
   *
   * @param reader The new input stream.
   */
  public final void yyreset(java.io.Reader reader) {
    zzReader = reader;
    zzEOFDone = false;
    yyResetPosition();
    zzLexicalState = YYINITIAL;
    int initBufferSize = Math.min(ZZ_BUFFERSIZE, zzMaxBufferLen());
    if (zzBuffer.length > initBufferSize) {
      zzBuffer = new char[initBufferSize];
    }
  }

  /**
   * Resets the input position.
   */
  private final void yyResetPosition() {
      zzAtBOL  = true;
      zzAtEOF  = false;
      zzCurrentPos = 0;
      zzMarkedPos = 0;
      zzStartRead = 0;
      zzEndRead = 0;
      zzFinalHighSurrogate = 0;
      yyline = 0;
      yycolumn = 0;
      yychar = 0L;
  }


  /**
   * Returns whether the scanner has reached the end of the reader it reads from.
   *
   * @return whether the scanner has reached EOF.
   */
  public final boolean yyatEOF() {
    return zzAtEOF;
  }


  /**
   * Returns the current lexical state.
   *
   * @return the current lexical state.
   */
  public final int yystate() {
    return zzLexicalState;
  }


  /**
   * Enters a new lexical state.
   *
   * @param newState the new lexical state
   */
  public final void yybegin(int newState) {
    zzLexicalState = newState;
  }


  /**
   * Returns the text matched by the current regular expression.
   *
   * @return the matched text.
   */
  public final String yytext() {
    return new String(zzBuffer, zzStartRead, zzMarkedPos-zzStartRead);
  }


  /**
   * Returns the character at the given position from the matched text.
   *
   * <p>It is equivalent to {@code yytext().charAt(pos)}, but faster.
   *
   * @param position the position of the character to fetch. A value from 0 to {@code yylength()-1}.
   *
   * @return the character at {@code position}.
   */
  public final char yycharat(int position) {
    return zzBuffer[zzStartRead + position];
  }


  /**
   * How many characters were matched.
   *
   * @return the length of the matched text region.
   */
  public final int yylength() {
    return zzMarkedPos-zzStartRead;
  }


  /**
   * Reports an error that occurred while scanning.
   *
   * <p>In a well-formed scanner (no or only correct usage of {@code yypushback(int)} and a
   * match-all fallback rule) this method will only be called with things that
   * "Can't Possibly Happen".
   *
   * <p>If this method is called, something is seriously wrong (e.g. a JFlex bug producing a faulty
   * scanner etc.).
   *
   * <p>Usual syntax/scanner level error handling should be done in error fallback rules.
   *
   * @param errorCode the code of the error message to display.
   */
  private static void zzScanError(int errorCode) {
    String message;
    try {
      message = ZZ_ERROR_MSG[errorCode];
    } catch (ArrayIndexOutOfBoundsException e) {
      message = ZZ_ERROR_MSG[ZZ_UNKNOWN_ERROR];
    }

    throw new Error(message);
  }


  /**
   * Pushes the specified amount of characters back into the input stream.
   *
   * <p>They will be read again by then next call of the scanning method.
   *
   * @param number the number of characters to be read again. This number must not be greater than
   *     {@link #yylength()}.
   */
  public void yypushback(int number)  {
    if ( number > yylength() )
      zzScanError(ZZ_PUSHBACK_2BIG);

    zzMarkedPos -= number;
  }




  /**
   * Resumes scanning until the next regular expression is matched, the end of input is encountered
   * or an I/O-Error occurs.
   *
   * @return the next token.
   * @exception java.io.IOException if any I/O-Error occurs.
   */
  public int yylex() throws java.io.IOException
  {
    int zzInput;
    int zzAction;

    // cached fields:
    int zzCurrentPosL;
    int zzMarkedPosL;
    int zzEndReadL = zzEndRead;
    char[] zzBufferL = zzBuffer;

    int [] zzTransL = ZZ_TRANS;
    int [] zzRowMapL = ZZ_ROWMAP;
    int [] zzAttrL = ZZ_ATTRIBUTE;

    while (true) {
      zzMarkedPosL = zzMarkedPos;

      yychar+= zzMarkedPosL-zzStartRead;

      boolean zzR = false;
      int zzCh;
      int zzCharCount;
      for (zzCurrentPosL = zzStartRead  ;
           zzCurrentPosL < zzMarkedPosL ;
           zzCurrentPosL += zzCharCount ) {
        zzCh = Character.codePointAt(zzBufferL, zzCurrentPosL, zzMarkedPosL);
        zzCharCount = Character.charCount(zzCh);
        switch (zzCh) {
        case '\u000B':  // fall through
        case '\u000C':  // fall through
        case '\u0085':  // fall through
        case '\u2028':  // fall through
        case '\u2029':
          yyline++;
          yycolumn = 0;
          zzR = false;
          break;
        case '\r':
          yyline++;
          yycolumn = 0;
          zzR = true;
          break;
        case '\n':
          if (zzR)
            zzR = false;
          else {
            yyline++;
            yycolumn = 0;
          }
          break;
        default:
          zzR = false;
          yycolumn += zzCharCount;
        }
      }

      if (zzR) {
        // peek one character ahead if it is
        // (if we have counted one line too much)
        boolean zzPeek;
        if (zzMarkedPosL < zzEndReadL)
          zzPeek = zzBufferL[zzMarkedPosL] == '\n';
        else if (zzAtEOF)
          zzPeek = false;
        else {
          boolean eof = zzRefill();
          zzEndReadL = zzEndRead;
          zzMarkedPosL = zzMarkedPos;
          zzBufferL = zzBuffer;
          if (eof)
            zzPeek = false;
          else
            zzPeek = zzBufferL[zzMarkedPosL] == '\n';
        }
        if (zzPeek) yyline--;
      }
      zzAction = -1;

      zzCurrentPosL = zzCurrentPos = zzStartRead = zzMarkedPosL;

      zzState = ZZ_LEXSTATE[zzLexicalState];

      // set up zzAction for empty match case:
      int zzAttributes = zzAttrL[zzState];
      if ( (zzAttributes & 1) == 1 ) {
        zzAction = zzState;
      }


      zzForAction: {
        while (true) {

          if (zzCurrentPosL < zzEndReadL) {
            zzInput = Character.codePointAt(zzBufferL, zzCurrentPosL, zzEndReadL);
            zzCurrentPosL += Character.charCount(zzInput);
          }
          else if (zzAtEOF) {
            zzInput = YYEOF;
            break zzForAction;
          }
          else {
            // store back cached positions
            zzCurrentPos  = zzCurrentPosL;
            zzMarkedPos   = zzMarkedPosL;
            boolean eof = zzRefill();
            // get translated positions and possibly new buffer
            zzCurrentPosL  = zzCurrentPos;
            zzMarkedPosL   = zzMarkedPos;
            zzBufferL      = zzBuffer;
            zzEndReadL     = zzEndRead;
            if (eof) {
              zzInput = YYEOF;
              break zzForAction;
            }
            else {
              zzInput = Character.codePointAt(zzBufferL, zzCurrentPosL, zzEndReadL);
              zzCurrentPosL += Character.charCount(zzInput);
            }
          }
          int zzNext = zzTransL[ zzRowMapL[zzState] + zzCMap(zzInput) ];
          if (zzNext == -1) break zzForAction;
          zzState = zzNext;

          zzAttributes = zzAttrL[zzState];
          if ( (zzAttributes & 1) == 1 ) {
            zzAction = zzState;
            zzMarkedPosL = zzCurrentPosL;
            if ( (zzAttributes & 8) == 8 ) break zzForAction;
          }

        }
      }

      // store back cached position
      zzMarkedPos = zzMarkedPosL;

      if (zzInput == YYEOF && zzStartRead == zzCurrentPos) {
        zzAtEOF = true;
              {
                if(getMarkerDocument().getContext().isInQuotes() && getMarkerDocument().getContext().isPlainQuotes()) {
		
		if(text.toString().length()> 0) {

			runService(text.toString());
		
			text = new StringBuilder();
		}
	}
	
	return YYEOF;
              }
      }
      else {
        switch (zzAction < 0 ? zzAction : ZZ_ACTION[zzAction]) {
          case 1:
            { if(getMarkerDocument().getContext().isInQuotes() && getMarkerDocument().getContext().isPlainQuotes()) {
	
		text.append(yytext());
	
	} else { 
	
		output.append(yytext());
	}
            }
          // fall through
          case 22: break;
          case 2:
            { output.append(yytext());
            }
          // fall through
          case 23: break;
          case 3:
            { text.append(yytext());
            }
          // fall through
          case 24: break;
          case 4:
            { yypushback(yylength());
	yybegin(inComma);
            }
          // fall through
          case 25: break;
          case 5:
            { output.append(text.toString()); //Trovato comma, svuota eventuale buffer articolo
		
		yypushback(yylength());
		yybegin(inComma);
            }
          // fall through
          case 26: break;
          case 6:
            { getMarkerDocument().getContext().resetCurrent();
		getMarkerDocument().getContext().updateContainer(yytext());

		output.append(yytext());
		
		text = new StringBuilder();
		
		yybegin(inCommaOpen);
            }
          // fall through
          case 27: break;
          case 7:
            { runService(text.toString());
		
		output.append(yytext());
		
		yybegin(inComma);
            }
          // fall through
          case 28: break;
          case 8:
            { output.append(yytext());
		
		yybegin(inComma);
            }
          // fall through
          case 29: break;
          case 9:
            { output.append(text.toString() + yytext()); //Trovata rubrica, svuota eventuale buffer testo
		
		text = new StringBuilder();
		
		yybegin(inArticleTitle);
            }
          // fall through
          case 30: break;
          case 10:
            { output.append(yytext());
		yybegin(inArticle);
            }
          // fall through
          case 31: break;
          case 11:
            { output.append(yytext());
	
	text = new StringBuilder();

	getMarkerDocument().getContext().resetCurrent();
	getMarkerDocument().getContext().updateContainer(yytext());
	
	yybegin(inArticle);
            }
          // fall through
          case 32: break;
          case 12:
            { runService(text.toString());
		
		output.append(yytext());
		
		yybegin(0);
            }
          // fall through
          case 33: break;
          case 13:
            { //Resetta il paragrafo in caso di assenza di commi dentro l'articolo
		getMarkerDocument().getContext().resetCurrent();
		
		output.append(yytext());
		
		yybegin(0);
            }
          // fall through
          case 34: break;
          case 14:
            { yypushback(yylength());
	yybegin(inParagraph);
            }
          // fall through
          case 35: break;
          case 15:
            { output.append(text.toString()); //Trovato paragrafo sotto articolo, svuota eventuale buffer articolo 

		yypushback(yylength());
		yybegin(inParagraph);
            }
          // fall through
          case 36: break;
          case 16:
            { output.append(text.toString()); //Trovato paragrafo, svuota eventuale buffer comma
		
		yypushback(yylength());
				
		yybegin(inParagraph);
            }
          // fall through
          case 37: break;
          case 17:
            { getMarkerDocument().getContext().resetCurrent();
		getMarkerDocument().getContext().updateContainer(yytext());

		output.append(yytext());
		
		text = new StringBuilder();
		
		yybegin(inParagraphOpen);
            }
          // fall through
          case 38: break;
          case 18:
            { runService(text.toString());
		
		output.append(yytext());
		
		yybegin(inParagraph);
            }
          // fall through
          case 39: break;
          case 19:
            { output.append(text.toString() + yytext()); //Trovata intestazione, svuota eventuale buffer testo
		
		text = new StringBuilder();
		
		yybegin(inArticleHeader);
            }
          // fall through
          case 40: break;
          case 20:
            { output.append(text.toString() + yytext()); //Trovata intestazione, svuota eventuale buffer testo
		
		text = new StringBuilder();
		
		yybegin(inCommaHeader);
            }
          // fall through
          case 41: break;
          case 21:
            { output.append(yytext());
		yybegin(inCommaOpen);
            }
          // fall through
          case 42: break;
          default:
            zzScanError(ZZ_NO_MATCH);
        }
      }
    }
  }

  /**
   * Runs the scanner on input files.
   *
   * This is a standalone scanner, it will print any unmatched
   * text to System.out unchanged.
   *
   * @param argv   the command line, contains the filenames to run
   *               the scanner on.
   */
  public static void main(String[] argv) {
    if (argv.length == 0) {
      System.out.println("Usage : java ReadPart [ --encoding <name> ] <inputfile(s)>");
    }
    else {
      int firstFilePos = 0;
      String encodingName = "UTF-8";
      if (argv[0].equals("--encoding")) {
        firstFilePos = 2;
        encodingName = argv[1];
        try {
          // Side-effect: is encodingName valid?
          java.nio.charset.Charset.forName(encodingName);
        } catch (Exception e) {
          System.out.println("Invalid encoding '" + encodingName + "'");
          return;
        }
      }
      for (int i = firstFilePos; i < argv.length; i++) {
        ReadPart scanner = null;
        java.io.FileInputStream stream = null;
        java.io.Reader reader = null;
        try {
          stream = new java.io.FileInputStream(argv[i]);
          reader = new java.io.InputStreamReader(stream, encodingName);
          scanner = new ReadPart(reader);
          while ( !scanner.zzAtEOF ) scanner.yylex();
        }
        catch (java.io.FileNotFoundException e) {
          System.out.println("File not found : \""+argv[i]+"\"");
        }
        catch (java.io.IOException e) {
          System.out.println("IO error scanning file \""+argv[i]+"\"");
          System.out.println(e);
        }
        catch (Exception e) {
          System.out.println("Unexpected exception:");
          e.printStackTrace();
        }
        finally {
          if (reader != null) {
            try {
              reader.close();
            }
            catch (java.io.IOException e) {
              System.out.println("IO error closing file \""+argv[i]+"\"");
              System.out.println(e);
            }
          }
          if (stream != null) {
            try {
              stream.close();
            }
            catch (java.io.IOException e) {
              System.out.println("IO error closing file \""+argv[i]+"\"");
              System.out.println(e);
            }
          }
        }
      }
    }
  }


}
